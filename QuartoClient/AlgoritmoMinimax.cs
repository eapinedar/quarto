﻿using Quarto.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoClient
{
    public class AlgoritmoMinimax
    {
        //public bool debug = true;


        /// <summary>
        /// Precondición: Tablero 
        /// </summary>
        /// <param name="tablero"></param>
        /// <param name="profundidad"></param>
        /// <returns></returns>
        public NodoQuarto ObtenerMovimiento(TableroMinimax tablero, int profundidad)
        {
            //Número de Nodos raíz a partir de los cuales se va a empezar a construir el árbol de posibles jugadas futuras.
            //Estructura de datos utilizada para el árbol: Arreglo de objetos NodoQuarto
            int numeronodosraiz = tablero.NumeroCasillasVacias * tablero.NumeroFichasDisponibles;
            NodoQuarto[] nodosraiz = new NodoQuarto[numeronodosraiz];

            //Construimos árbol de decisión a partir de las casilals vacías y las fichas que no han sido jugadas.
            int[] casillasvacias = tablero.ObtenerCasillasVacias();
            int[] fichasfaltantes = tablero.ObtenerFichasDisponibles();
            //Esta variable identifica la ficha que está en juego. Con el fin de iniciar la simulación de jugadas futuras (En cada casillas vacía)
            int fichapasada = tablero.fichapasada;
            //Para cada casilla vacía y por cada ficha que no ha sido jugada empezamos a construir el árbol del juego
            int m = 0;
            for (int i = 0; i < tablero.NumeroCasillasVacias; i++)
            {
                for (int j = 0; j < tablero.NumeroFichasDisponibles; j++)
                {
                    //En este caso enviamos una 'C' es decir que es nuestro turno... Con el fin de saber que estamos en un nodo de máximo
                    nodosraiz[m] = ConstruirArbol(fichapasada, casillasvacias[i], fichasfaltantes[j], tablero, 'C', profundidad);
                    m++;
                }
            }

            //// Mostrar arbol en debug
            //if (debug)
            //{
            //    for (int k = 0; k < m; k++)
            //    {
            //        string cadena = string.Empty ;
            //        Console.WriteLine("RAÍZ:" + MostrarArbol(nodosraiz[k], ref cadena));
            //    }
            //}

            //// Computar minimax
            //if (debug) Console.Write(" Computando Minimax: ");
            int[] resultados = new int[numeronodosraiz];
            int res = 0;
            //Listado de mejores nodos, usado para entregar un movimiento al azar en caso de que hayan nodos con pesos similares
            Random random = new Random(DateTime.Now.Millisecond);
            int alea = random.Next(10, 50);
            int contador = 0;
            int definitivoAleatorio = 0;
            for (int x = 0; x < numeronodosraiz; x++)
            {
                resultados[x] = ComputarMiniMax(nodosraiz[x]);

                //if (debug) Console.WriteLine(nodosraiz[x].CadenaArbol() + "=" + resultados[x] + ((x < (numeronodosraiz - 1)) ? "," : " "));

                // Minimax res=0
                if (x > 0 && resultados[x] > resultados[res])
                {
                    contador = 0;
                    res = x;
                }
                if (x > 0 && resultados[x] == resultados[res])
                {
                    contador++;
                    if (contador <= alea)
                    {
                        definitivoAleatorio = x;
                    }
                    //res =  ENVIAR  AL AZAR los mejores nodos si tienen varios el mismo peso

                    //mejoresNodos.Add(x);
                }
            }

            //if (mejoresNodos.Count > 1)
            //{
            //    Random objrandom = new Random();
            //    res = (int)(objrandom.NextDouble() * mejoresNodos.Count);
            //}

            //  if (debug) Console.WriteLine();

            return nodosraiz[definitivoAleatorio];
        }

        /// <summary>
        /// Método encargado de Construir el árbol del juego
        /// </summary>
        /// <param name="fichapasada"></param>
        /// <param name="casilla"></param>
        /// <param name="siguienteficha"></param>
        /// <param name="EstadoTablero"></param>
        /// <param name="jugador"></param>
        /// <param name="profundidad"></param>
        /// <returns></returns>
        public NodoQuarto ConstruirArbol(int fichapasada, int casilla, int siguienteficha, TableroMinimax EstadoTablero, char jugador, int profundidad)
        {
            try
            {
                NodoQuarto nodo = new NodoQuarto(casilla, siguienteficha, jugador);
                //Este objeto es utilizado por el algoritmo para poder simular jugadas futuras
                TableroMinimax tablerosimulado = new TableroMinimax();

               //Se realiza una clonación del Estado del Tablero actual, lo que se desea hacer es empezar a realizar todas las posibles combinaciones de jugadas con el fin 
               //de poder construir el árbol de jugadas simuladas y realizar el cómputo de minimax.
                tablerosimulado = Utils.CopiaProfunda<TableroMinimax>(EstadoTablero);

                //Realizamos una jugada simulada en una de las posibles casillas vacías
                tablerosimulado.JugarFichaPasada(casilla);

                //Realizamos validación de si hubo Quarto, con el fin de marcar el nodo como hoja y nodo donde hubo quarto
                if (tablerosimulado.VerificarQuarto(casilla))
                {
                    nodo.esHoja = true;
                    nodo.esQuarto = true;
                    return nodo;
                }

                //Valida Si ya se validaron todas las fichas disponibles con el fin de marcar el nodo como hoja
                if (siguienteficha == 16)
                {
                    nodo.esHoja = true;
                    return nodo;
                }

                //Valida si ya se ha a alcanzado la búsqueda en profundidad del  grafo  deseada.
                if (profundidad == 0)
                {
                    // Profundidad deseada alcanzada
                    nodo.esHoja = true;
                    return nodo;
                }

                //Construir nodos hijos
                tablerosimulado.EntregarFicha(siguienteficha);

                int[] casillasvacias = tablerosimulado.ObtenerCasillasVacias();
                int[] fichassinjugar = tablerosimulado.ObtenerFichasDisponibles();

                //EPINEDA 19/NOV/2018 Fix Bug
                for (int i = 0; i < tablerosimulado.NumeroCasillasVacias; i++)
                {
                    if (fichassinjugar.Length > 0)
                    {
                        for (int j = 0; j < tablerosimulado.NumeroFichasDisponibles; j++)
                        {
                            //Recursión. Cambio de jugador
                            nodo.AgregarHijo(ConstruirArbol(siguienteficha, casillasvacias[i],
                                                           fichassinjugar[j], tablerosimulado, (jugador == 'C') ? 'U' : 'C', profundidad - 1));
                        }
                    }
                    else  // no hay mas fichas = nodo hoja. Se envía 16 cómo parámetro para indicar que no hay más fichas disponibles
                        nodo.AgregarHijo(ConstruirArbol(siguienteficha, casillasvacias[i], 16, tablerosimulado, (jugador == 'C') ? 'U' : 'C', profundidad - 1));
                }

                return nodo;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string MostrarArbol(NodoQuarto raiz, ref string cadena)
        {

            cadena += raiz.CadenaArbol();

            if (!raiz.esHoja)
            {
                cadena += "-(";

                foreach (var hijo in raiz.nodoshijo)
                {
                    MostrarArbol(hijo, ref cadena);
                }
                cadena += ")";
            }

            return cadena;
        }

        /// <summary>
        /// Este es el método más importante del algoritmo minimax. Recibe como Input un Nodo del árbol del juego, y le da un puntaje o peso dependiendo  de si
        /// es una victoria para el computador o para nuestro adversario.
        /// </summary>
        /// <param name="nodo"></param>
        /// <returns></returns>
        private int ComputarMiniMax(NodoQuarto nodo)
        {
            
            if (nodo.esHoja)
            {
                if (nodo.esQuarto)
                    //Heurística: Retorna un  valor negativo si es una derrota para  nuestro jugador, un valor positivo si es una victoria...  y cero si es un empate
                    return (nodo.jugador == 'C') ? 1 : -1;
                else
                    return 0;
            }

            int valortemporal, valorfinal = 0;

            foreach (var nodohijo in nodo.nodoshijo)
            {
                valortemporal = ComputarMiniMax(nodohijo);

                if (nodo.jugador == 'C')
                {
                    valorfinal = Math.Min(valorfinal, valortemporal);
                }
                else
                {
                    valorfinal = Math.Max(valorfinal, valortemporal);
                }
            }
            return valorfinal;
        }
    }
}
