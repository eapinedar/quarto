﻿using Quarto.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoClient
{
    public class QuartoCliente
    {
        static void Main(string[] args)
        {
            //1.crear partida
            string nombrepartida = "Partida Mía Mariana 3";
            string nombrejugador = "Andrés Pineda";
            ViewModelPartida objpartida = CrearPartida(nombrepartida, nombrejugador).Result;
            RealizarMovimientoJugador1(objpartida.IdPartida, objpartida.IdJugador).Wait();


            ////1.Unirse Partida
            //string idpartida = "5c09d61cf17c226b7404c935";
            //string nombrejugador2 = "Andres";
            //ViewModelPartida objpartidaunirse = UnirsePartida(idpartida, nombrejugador2).Result;
            //string idjugador2 = objpartidaunirse.IdJugador;
            ////RealizarMovimientoJugador2(idpartida, "096a7516-4a9f-44d0-b1f9-419ebe1fc717").Wait();
            //RealizarMovimientoJugador1(idpartida, idjugador2).Wait();


            // //// nuevo 02 dic
            // string idpartida = "5c047193f17c195710a86e33";
            // string idjugador1 = "187c6967-68df-433d-860a-95aa2c151432";
            //// string idjugador2 = "293503ac-e55c-4711-8e89-6106a726adb2";
            // RealizarMovimientoJugador1(idpartida, idjugador1).Wait();
            // //RealizarMovimientoJugador1(idpartida, idjugador2).Wait();

            //// nuevo 02 dic
            //string idpartida = objpartida.IdPartida;
            //string idjugador1 = "187c6967-68df-433d-860a-95aa2c151432";
            //string idjugador1 = objpartida.IdJugador;
            // string idjugador2 = "293503ac-e55c-4711-8e89-6106a726adb2";
            //RealizarMovimientoJugador1(objpartida.IdPartida, objpartida.IdJugador).Wait();


        }

        private static async Task<ViewModelPartida> CrearPartida(string nombrepartida, string nombrejugador)
        {
            ApiService apiService = new ApiService();

            //string idjugador = String.Empty;
            //string idPartida = String.Empty;

            var response = await apiService.CrearPartida<ViewModelPartida>("http://quartopoli.eastus.cloudapp.azure.com",
                                                                                     "/api/Quarto", "/CrearPartida",
                                                                                     nombrepartida, nombrejugador);

            ViewModelPartida resultadoPartida = (ViewModelPartida)response.Result;

            //if (!resultadoPartida.Respuesta.Error)
            //{
            //idjugador = resultadoPartida.IdJugador;
            //idPartida = resultadoPartida.IdPartida;
            return resultadoPartida;
            //}
            //else
            //{
            //    string error = resultadoPartida.Respuesta.Mensaje;
            //}


        }

        private static async Task<ViewModelPartida> UnirsePartida(string idpartida, string nombreusuario)
        {
            ApiService servicioAPI = new ApiService();

            //string idjugador = String.Empty;
            //string idPartida = String.Empty;

            var response = await servicioAPI.UnirsePartida<ViewModelPartida>("http://quartopoli.eastus.cloudapp.azure.com",
                                                                                     "/api/Quarto", "/UnirsePartida",
                                                                                     idpartida, nombreusuario);

            ViewModelPartida resultadoPartida = (ViewModelPartida)response.Result;

            //if (!resultadoPartida.Respuesta.Error)
            //{
            //idjugador = resultadoPartida.IdJugador;
            //idPartida = resultadoPartida.IdPartida;
            return resultadoPartida;
            //}
            //else
            //{
            //    string error = resultadoPartida.Respuesta.Mensaje;
            //}


        }

        /// <summary>
        /// Método encargado de realizar un movimiento de Quarto a través del Web Service, adicionalmente es encargado de
        /// realizar las respectivas conversiones del tablero enviado por el Web Service. Para poder usar el algoritmo minimax con una profundidad de acuerdo
        /// al estado del tablero
        /// </summary>
        /// <param name="idpartida"></param>
        /// <param name="idjugador"></param>
        /// <returns></returns>
        public static async Task RealizarMovimientoJugador1(string idpartida, string idjugador)
        {
            try
            {
                ApiService servicioAPI = new ApiService();
                //Variable centinela o Flag encargada de definir cuando se terminó una partida y por lo tanto no se debe realizar mas consumos al WS.
                var partidafinalizada = false;

                // { Pre Q:  partidafinalizada = false
                do
                {
                    var response = await servicioAPI.GetEstadoPartida<ViewModelEstadoPartida>("http://quartopoli.eastus.cloudapp.azure.com",
                                                     "/api/Quarto", "/VerificarEstadoPartida",
                                                     idpartida, idjugador);

                    ViewModelEstadoPartida resultadoPartida = (ViewModelEstadoPartida)response.Result;
                    ViewModelMovimiento movimiento;
                    partidafinalizada = resultadoPartida.PartidaFinalizada;

                    //Verificar que debo jugador
                    if (resultadoPartida.TurnoJugador)
                    {
                        //Si es el primer movimiento, no usar minimax. Entregar cualquier ficha
                        if (resultadoPartida.Tablero.Fichas.Where(mm => mm.Disponible).ToList().Count == 16)
                        {
                            movimiento = new ViewModelMovimiento
                            {
                                IdPartida = idpartida,
                                IdJugador = idjugador,
                                FichaEntregar = new ViewModelFicha { Alta = true, Blanca = true, Cuadrada = true, Llena = true }
                            };
                        }
                        else
                        {

                            //Uso de algoritmo MINIMAX 
                            TableroMinimax objtablero = new TableroMinimax();
                            AlgoritmoMinimax minimax = new AlgoritmoMinimax();

                            //Conversión para poder utilizar el algoritmo, se utilizan arreglos para poder almacenar las fichas y las casillas del tablero.
                            int[] estadoficha = new int[16];
                            int[] estadocasillas = new int[16];
                            int fichasdisponibles = 0;
                            int casillasporllenar = 0;

                            int indfichas = 0;
                            foreach (var item in resultadoPartida.Tablero.Fichas)
                            {
                                if (item.Disponible)
                                {
                                    estadoficha[indfichas] = 0;
                                    fichasdisponibles++;
                                }
                                else
                                {
                                    estadoficha[indfichas] = 2;
                                }
                                indfichas++;
                            }

                            objtablero.estadoficha = estadoficha;

                            int indcasilla = 0;

                            foreach (var objcasilla in resultadoPartida.Tablero.Casillas)
                            {
                                if (objcasilla == null)
                                {
                                    estadocasillas[indcasilla] = 16;
                                    casillasporllenar++;
                                }
                                else
                                {
                                    //Posición arreglo de la ficha
                                    estadocasillas[indcasilla] = objcasilla.IdentificadorFicha;
                                }
                                indcasilla++;
                            }

                            objtablero.estadotablero = estadocasillas;

                            objtablero.fichapasada = resultadoPartida.FichaEnJuego.IdentificadorFicha;
                            objtablero.NumeroFichasDisponibles = fichasdisponibles;
                            objtablero.NumeroCasillasVacias = casillasporllenar;

                            //Este algoritmo .. se comporta bien (tiempo de ejecución) con profundidad >= 8 cuando faltan 6 casillas vacías en el tablero.
                            //Como posterior mejora se desea implementar Poda alfa beta con el fin de mejorar el rendimiento y Memoization para almacenar jugadas y no volver a calcular las mismas
                            NodoQuarto nodorespuesta;
                            if (casillasporllenar <= 6)
                            {
                                nodorespuesta = minimax.ObtenerMovimiento(objtablero, 8);
                            }
                            else
                            {
                                nodorespuesta = minimax.ObtenerMovimiento(objtablero, 1);
                            }
                           
                            // Realizar movimiento de acuerdo a minimax
                            int posfila = nodorespuesta.casilla / 4;
                            int poscolumna = nodorespuesta.casilla % 4;
                            Ficha fichaajugar = resultadoPartida.Tablero.Fichas.Where(mm => mm.IdentificadorFicha == nodorespuesta.fichapasada).FirstOrDefault();

                            movimiento = new ViewModelMovimiento
                            {
                                IdPartida = idpartida,
                                IdJugador = idjugador,
                                Ubicaciontableroindicefila = posfila,
                                Ubicaciontableroindicecolumna = poscolumna,

                                FichaEntregar = new ViewModelFicha
                                {
                                    Alta = fichaajugar.Alta,
                                    Blanca = fichaajugar.Blanca,
                                    Cuadrada = fichaajugar.Cuadrada,
                                    Llena = fichaajugar.Llena
                                }
                            };


                        }
                        //Realizar movimiento
                        var responsemov = await servicioAPI.Post("http://quartopoli.eastus.cloudapp.azure.com",
                                                                 "/api/Quarto", "/RealizarMovimiento", movimiento);


                    }
                    else
                    {
                        string soloparacontrolardebug = "NO ES MI TURNO";
                    }
                } while (!partidafinalizada);

                // { Pos R: partidafinalizada = true

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

    }
}
