﻿using Quarto.Domain;
using System;
using System.Collections.Generic;

namespace QuartoClientAlpha_BethaPruning
{
    public class AlgoritmoMinimaxJugador2
    {
        public bool debug = true;

        public NodoQuarto ObtenerMovimiento(TableroMinimax tablero, int profundidad)
        {
            int numeronodosraiz = tablero.NumeroCasillasVacias * tablero.NumeroFichasDisponibles;
            NodoQuarto[] nodosraiz = new NodoQuarto[numeronodosraiz];

            //Construimos árbol de decisión
            int[] casillasvacias = tablero.ObtenerCasillasVacias();
            int[] fichasfaltantes = tablero.ObtenerFichasDisponibles();
            int fichapasada = tablero.fichapasada;

            int m = 0;
            for (int i = 0; i < tablero.NumeroCasillasVacias; i++)
            {
                for (int j = 0; j < tablero.NumeroFichasDisponibles; j++)
                {
                    nodosraiz[m] = ConstruirArbol(fichapasada, casillasvacias[i], fichasfaltantes[j], tablero, 'C', profundidad);
                    m++;
                }
            }

            //// Mostrar arbol en debug
            //if (debug)
            //{
            //    for (int k = 0; k < m; k++)
            //    {
            //        string cadena = string.Empty ;
            //        Console.WriteLine("RAÍZ:" + MostrarArbol(nodosraiz[k], ref cadena));
            //    }
            //}

            //// Computar minimax
            //if (debug) Console.Write(" Computando Minimax: ");
            int[] resultados = new int[numeronodosraiz];
            int res = 0;
            //Listado de mejores nodos, usado para entregar un movimiento al azar en caso de que hayan nodos con pesos similares
            List<int> mejoresNodos = new List<int>
            {
                0
            };
            Random rnd = new Random(DateTime.Now.Millisecond);
            int alea = rnd.Next(10, 50);
            int contador = 0;
            int definitivoAleatorio = 0;
            for (int x = 0; x < numeronodosraiz; x++)
            {
                resultados[x] = ComputarMiniMax(nodosraiz[x]);

                //if (debug) Console.WriteLine(nodosraiz[x].GetString() + "=" + resultados[x] + ((x < (numeronodosraiz - 1)) ? "," : " "));


                // Minimax res=0
                if (x > 0 && resultados[x] > resultados[res])
                {
                    contador = 0;
                    res = x;
                }
                if (x > 0 && resultados[x] == resultados[res])
                {
                    contador++;
                    if (contador <= alea)
                    {
                        definitivoAleatorio = x;
                    }
                    //res =  ENVIAR  AL AZAR los mejores nodos si tienen varios el mismo peso

                    //mejoresNodos.Add(x);
                }
            }



            //if (mejoresNodos.Count > 1)
            //{
            //    Random objrandom = new Random();
            //    res = (int)(objrandom.NextDouble() * mejoresNodos.Count);
            //}

            //  if (debug) Console.WriteLine();

            return nodosraiz[definitivoAleatorio];
        }

        public NodoQuarto ConstruirArbol(int fichapasda, int casilla, int siguienteficha, TableroMinimax EstadoTablero, char jugador, int profundidad)
        {
            try
            {
                NodoQuarto nodo = new NodoQuarto(casilla, siguienteficha, jugador);
                TableroMinimax tablerosimulado = new TableroMinimax();

                //cboard = EstadoTablero;
                //cboard = (TableroMinimax)EstadoTablero.Clone(); INTENTO 1 SIRVIO PA CHIMBAS porque matrix es uan variable por referencia
                tablerosimulado = Utils.DeepCopy<TableroMinimax>(EstadoTablero);

                //for (int i = 0; i < EstadoTablero.estadotablero.Length; i++)
                //{
                //        cboard.estadotablero[i] = EstadoTablero.estadotablero[i];
                //}

                //for (int i = 0; i < EstadoTablero.estadoficha.Length; i++)
                //{
                //    cboard.estadoficha[i] = EstadoTablero.estadoficha[i];
                //}



                if (!tablerosimulado.JugarFichaPasada(casilla))
                    Console.WriteLine("Error en testeo al construir árbol. (Minimax)");

                if (tablerosimulado.VerificarQuarto(casilla))
                {
                    nodo.esHoja = true;
                    nodo.esQuarto = true;
                    return nodo;
                }

                if (siguienteficha == 16)
                {
                    nodo.esHoja = true;
                    return nodo;
                }

                if (profundidad == 0)
                {
                    // Profundidad deseada alcanzada
                    nodo.esHoja = true;
                    return nodo;
                }

                //Construir nodos hijos
                tablerosimulado.EntregarFicha(siguienteficha);
                //AQUI VAMOS BIEN
                int[] casillasvacias = tablerosimulado.ObtenerCasillasVacias();
                int[] fichassinjugar = tablerosimulado.ObtenerFichasDisponibles();

                //EPINEDA 19/NOV/2018 Fix Bug
                for (int i = 0; i < tablerosimulado.NumeroCasillasVacias; i++)
                {
                    if (fichassinjugar.Length > 0)
                    {
                        for (int j = 0; j < tablerosimulado.NumeroFichasDisponibles; j++)
                        {
                            //Recursión. Cambio de jugador
                            nodo.AgregarHijo(ConstruirArbol(siguienteficha, casillasvacias[i],
                                                           fichassinjugar[j], tablerosimulado, (jugador == 'C') ? 'U' : 'C', profundidad - 1));
                        }
                    }
                    else  // no hay mas fichas = nodo hoja, use a placeholder for nextPiece
                        nodo.AgregarHijo(ConstruirArbol(siguienteficha, casillasvacias[i], 16, tablerosimulado, (jugador == 'C') ? 'U' : 'C', profundidad - 1));
                }

                return nodo;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public string MostrarArbol(NodoQuarto raiz, ref string cadena)
        {

            cadena += raiz.CadenaArbol();

            if (!raiz.esHoja)
            {
                cadena += "-(";

                foreach (var hijo in raiz.nodoshijo)
                {
                    MostrarArbol(hijo, ref cadena);
                }
                cadena += ")";
            }

            return cadena;
        }

        private int ComputarMiniMax(NodoQuarto nodo)
        {
            if (nodo.esHoja)
            {
                if (nodo.esQuarto)
                    return (nodo.jugador == 'C') ? 1 : -1;
                else
                    return 0;
            }

            int tans, ans = 0;

            foreach (var nodohijo in nodo.nodoshijo)
            {
                tans = ComputarMiniMax(nodohijo);

                if (nodo.jugador == 'C')
                {
                    ans = Math.Min(ans, tans);
                }
                else
                {
                    ans = Math.Max(ans, tans);
                }
            }
            return ans;
        }
    }
}