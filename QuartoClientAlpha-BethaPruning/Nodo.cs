﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QuartoClientAlpha_BethaPruning.Piece;

namespace QuartoClientAlpha_BethaPruning
{
    public class Nodo
    {
        private Board gameState;
        private List<Nodo> children;
        private Boolean isMe;
        private Boolean isPlace;

        private int lastPlace = -1;

        public Nodo(Board gameState, Boolean isMe, Boolean isPlace, int place)
        {
            this.gameState = gameState;
            this.isMe = isMe;
            this.isPlace = isPlace;
            this.lastPlace = place;
        }

        //public int MejorCasilla(int profundidad, Boolean usarheuristica)
        //{
        //    List<int> mejoresCasillas = new List<int>();
        //    int bestValue = -10000;

        //    //    int size = gameState.GetCasillasVacias().Count;
        //    //    for (int i = 0; i < size; i++)
        //    //    {
        //    //        Board newGameState = new SuperBoard();
        //    //        ((SuperBoard)newGameState).setSuper((SuperBoard)gameState);
        //    //        newGameState.setBoard(gameState.getBoard());
        //    //        newGameState.setPieces(gameState.getPieces());
        //    //        newGameState.placePiece(gameState.getFreePlaces().get(i), gameState.getActivePiece());
        //    //        SuperNode newNode = new SuperNode(newGameState, true, false, gameState.getFreePlaces().get(i));
        //    //        int newValue = newNode.getValue(depth - 1, -10000, 10000, true, heuristic);
        //    //        if (newValue > bestValue)
        //    //        {
        //    //            bestValue = newValue;
        //    //            bestPlaces.clear();
        //    //            bestPlaces.add(gameState.getFreePlaces().get(i));
        //    //        }
        //    //        else if (newValue == bestValue)
        //    //        {
        //    //            bestPlaces.add(gameState.getFreePlaces().get(i));
        //    //        }
        //    //    }

        //    //return bestPlaces.get((int)Math.floor(bestPlaces.size() * Math.random()));
        //}
        private bool VerificarVictoria()
        {
            //int col = lastPlace % 4;
            //int row = (int)Math.Floor((double)lastPlace / 4);
            //if (VerificarFila(gameState.GetColumna(col))) return true;
            //if (VerificarFila(gameState.getRow(row))) return true;
            //if (col == row && checkRow(gameState.getDiagonalRight())) return true;
            //else if (col + row == 3 && checkRow(gameState.getDiagonalLeft())) return true;

            return false;
        }
        private bool VerificarFila(List<Piece> fichas)
        {
            if (fichas[0] == null) return false;
            Color c = fichas[0].getColor();
            Height h = fichas[0].getHeight();
            Shape s = fichas[0].getShape();
            Consistensy cons = fichas[0].getConsistency();
            bool color = true;
            bool height = true;
            bool shape = true;
            bool consistency = true;
            for (int i = 1; i < 4; i++)
            {
                if (fichas[i] == null) return false;
                if (color && fichas[i].getColor() != c) color = false;
                if (height && fichas[i].getHeight() != h) height = false;
                if (shape && fichas[i].getShape() != s) shape = false;
                if (consistency && fichas[i].getConsistency() != cons) consistency = false;
            }
            return color || height || shape || consistency;
        }
    }
}
