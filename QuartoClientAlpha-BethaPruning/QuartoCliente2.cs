﻿using Quarto.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoClientAlpha_BethaPruning
{
    public class QuartoCliente2
    {
        static void Main(string[] args)
        {
            //string idjugador2 = "293503ac-e55c-4711-8e89-6106a726adb2";


            //1.Unirse Partida
            string idpartida = "5c09ac41f17c226b7404c908";
            string nombrejugador2 = "And";
            ViewModelPartida objpartidaunirse = UnirsePartida(idpartida, nombrejugador2).Result;
            string idjugador2 = objpartidaunirse.IdJugador;
            //   RealizarMovimientoJugador2(idpartida, "096a7516-4a9f-44d0-b1f9-419ebe1fc717").Wait();
            RealizarMovimientoJugador2(idpartida, idjugador2).Wait();

        }

        private static async Task<ViewModelPartida> UnirsePartida(string idpartida, string nombreusuario)
        {
            ApiService apiService = new ApiService();

            //string idjugador = String.Empty;
            //string idPartida = String.Empty;

            var response = await apiService.UnirsePartida<ViewModelPartida>("http://quartopoli.eastus.cloudapp.azure.com",
                                                                                     "/api/Quarto", "/UnirsePartida",
                                                                                     idpartida, nombreusuario);

            ViewModelPartida resultadoPartida = (ViewModelPartida)response.Result;

            //if (!resultadoPartida.Respuesta.Error)
            //{
            //idjugador = resultadoPartida.IdJugador;
            //idPartida = resultadoPartida.IdPartida;
            return resultadoPartida;
            //}
            //else
            //{
            //    string error = resultadoPartida.Respuesta.Mensaje;
            //}


        }

        public static async Task RealizarMovimientoJugador2(string idpartida, string idjugador)
        {
            try
            {
                ApiService apiService = new ApiService();
                var partidafinalizada = false;

                do
                {
                    var response = await apiService.GetEstadoPartida<ViewModelEstadoPartida>("http://quartopoli.eastus.cloudapp.azure.com",
                                                     "/api/Quarto", "/VerificarEstadoPartida",
                                                     idpartida, idjugador);

                    ViewModelEstadoPartida resultadoPartida = (ViewModelEstadoPartida)response.Result;
                    ViewModelMovimiento movimiento;
                    partidafinalizada = resultadoPartida.PartidaFinalizada;

                    if (resultadoPartida.TurnoJugador)
                    {
                        //Si es el primer movimiento, no usar minimax. Entregar Ficha a lo que salga
                        if (resultadoPartida.Tablero.Fichas.Where(mm => mm.Disponible).ToList().Count == 16)
                        {
                            movimiento = new ViewModelMovimiento
                            {
                                IdPartida = idpartida,
                                IdJugador = idjugador,
                                FichaEntregar = new ViewModelFicha { Alta = true, Blanca = true, Cuadrada = true, Llena = true }
                            };
                        }//Si quedan cero fichass disponibles.. solo hay una caslla donde poner la ficha. No es necesario computar minimax
                        else if (resultadoPartida.Tablero.Fichas.Where(mm => mm.Disponible).ToList().Count == 0)
                        {
                            int posfila = -1;
                            int poscolumna = -1;

                            for (int fila = 0; fila < resultadoPartida.Tablero.Casillas.GetLength(0); fila++)
                            {
                                for (int columna = 0; columna < resultadoPartida.Tablero.Casillas.GetLength(1); columna++)
                                {
                                    if (resultadoPartida.Tablero.Casillas[fila, columna] == null)
                                    {
                                        //última Casilla encontrada
                                        posfila = fila;
                                        poscolumna = columna;
                                    }
                                }
                            }

                            movimiento = new ViewModelMovimiento
                            {
                                IdPartida = idpartida,
                                IdJugador = idjugador,
                                FichaEntregar = new ViewModelFicha()
                                {
                                    Alta = resultadoPartida.FichaEnJuego.Alta,
                                    Blanca = resultadoPartida.FichaEnJuego.Blanca,
                                    Cuadrada = resultadoPartida.FichaEnJuego.Cuadrada,
                                    Llena = resultadoPartida.FichaEnJuego.Llena
                                },
                                Ubicaciontableroindicefila = posfila,
                                Ubicaciontableroindicecolumna = poscolumna,

                            };
                        }
                        else
                        {

                            //Uso de algoritmo MINIMAX 
                            TableroMinimax objtablero = new TableroMinimax();
                            AlgoritmoMinimaxJugador2 minimax = new AlgoritmoMinimaxJugador2();

                            //TODO Conversión temporal
                            int[] estadoficha = new int[16];
                            int[] estadocasillas = new int[16];
                            int fichasfaltantes = 0;
                            int casillasporllenar = 0;

                            int indfichas = 0;
                            foreach (var item in resultadoPartida.Tablero.Fichas)
                            {
                                if (item.Disponible)
                                {
                                    estadoficha[indfichas] = 0;
                                    fichasfaltantes++;
                                }
                                else
                                {
                                    estadoficha[indfichas] = 2;
                                }
                                indfichas++;
                            }

                            objtablero.estadoficha = estadoficha;

                            int indcasilla = 0;

                            foreach (var objcasilla in resultadoPartida.Tablero.Casillas)
                            {
                                if (objcasilla == null)
                                {
                                    estadocasillas[indcasilla] = 16;
                                    casillasporllenar++;
                                }
                                else
                                {
                                    //  estadocasillas[indcasilla] = int.Parse(objcasilla.Alta.ToString()) * 8 + int.Parse(objcasilla.Blanca.ToString()) * 4
                                    //                    + int.Parse(objcasilla.Cuadrada.ToString()) * 2 + int.Parse(objcasilla.Llena.ToString());
                                    estadocasillas[indcasilla] = objcasilla.IdentificadorFicha;
                                }
                                indcasilla++;
                            }

                            objtablero.estadotablero = estadocasillas;

                            objtablero.fichapasada = resultadoPartida.FichaEnJuego.IdentificadorFicha;
                            objtablero.NumeroFichasDisponibles = fichasfaltantes;
                            objtablero.NumeroCasillasVacias = casillasporllenar;


                            //Este algoritmo .. se comporta bien con profundidad >= 8 cuando faltan 6 casillas vacías en el tablero
                            NodoQuarto nodorespuesta;
                            if (casillasporllenar <= 6)
                            {
                                nodorespuesta = minimax.ObtenerMovimiento(objtablero, 8);
                            }
                            else
                            {
                                nodorespuesta = minimax.ObtenerMovimiento(objtablero, 1);
                            }

                            // TODO Ralizar movimiento de acuerdo a minimax
                            //TODO Conversión temporal
                            int posfila = nodorespuesta.casilla / 4;
                            int poscolumna = nodorespuesta.casilla % 4;
                            Ficha fichaajugar = resultadoPartida.Tablero.Fichas.Where(mm => mm.IdentificadorFicha == nodorespuesta.fichapasada).FirstOrDefault();

                            movimiento = new ViewModelMovimiento
                            {
                                IdPartida = idpartida,
                                IdJugador = idjugador,
                                Ubicaciontableroindicefila = posfila,
                                Ubicaciontableroindicecolumna = poscolumna,

                                FichaEntregar = new ViewModelFicha
                                {
                                    Alta = fichaajugar.Alta,
                                    Blanca = fichaajugar.Blanca,
                                    Cuadrada = fichaajugar.Cuadrada,
                                    Llena = fichaajugar.Llena
                                }
                            };


                        }
                        //Realizar movimiento
                        var responsemov = await apiService.Post("http://quartopoli.eastus.cloudapp.azure.com",
                                                                 "/api/Quarto", "/RealizarMovimiento", movimiento);


                    }
                    else
                    {
                        string esperandoabeto = "LALOPENDEJO";
                    }
                } while (!partidafinalizada);




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
