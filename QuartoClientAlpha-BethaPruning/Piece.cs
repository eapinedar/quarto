﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoClientAlpha_BethaPruning
{
    public class Piece
    {
        public enum Color { WHITE, BLACK };
        public enum Height { TALL, SHORT };
        public enum Shape { SQUARE, ROUND }
        public enum Consistensy { HOLLOW, SOLID };

        private Color color;
        private Height height;
        private Shape shape;
        private Consistensy consistensy;
        private String id;

        public Piece(Color c, Height h, Shape s, Consistensy con)
        {
            this.color = c;
            this.height = h;
            this.shape = s;
            this.consistensy = con;
            if (this.color.Equals(Color.WHITE))
            {
                this.id = "w";
            }
            else
            {
                this.id = "b";
            }
            if (this.height.Equals(Height.TALL))
            {
                this.id = this.id.ToUpper();
            }
            if (this.shape.Equals(Shape.ROUND))
            {
                this.id = "(" + this.id + ")";
            }
            if (this.consistensy.Equals(Consistensy.HOLLOW))
            {
                this.id = this.id + "*";
            }
            if (id.Length == 1)
            {
                this.id = " " + this.id + "  ";
            }
            else if (id.Length == 2)
            {
                this.id = " " + this.id + " ";
            }
            else if (id.Length == 3)
            {
                this.id = this.id + " ";
            }
        }

        public Color getColor()
        {
            return color;
        }

        public void setColor(Color color)
        {
            this.color = color;
        }

        public Height getHeight()
        {
            return height;
        }

        public void setHeight(Height height)
        {
            this.height = height;
        }

        public Shape getShape()
        {
            return shape;
        }

        public void setShape(Shape shape)
        {
            this.shape = shape;
        }

        public Consistensy getConsistency()
        {
            return consistensy;
        }

        public void setConsistensy(Consistensy consistensy)
        {
            this.consistensy = consistensy;
        }

        public String toString()
        {

            return this.id;
        }


    }
}
