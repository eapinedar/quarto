﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoClientAlpha_BethaPruning
{
    public class Board
    {
        private bool[] winningPieceSorts = new bool[8];
        private bool[,] possibleWins = new bool[10,8];
        private int[] pieceCounts = new int[10];
        //bool[,] possibleWins = new bool[10];
        private List<Piece> tablero = new List<Piece>();
        private List<Piece> pieces = new List<Piece>();
        private Piece activePiece;
        private bool isPlace;


        private List<Piece> notWinningPieces = new List<Piece>();

        public Board()
        {
            this.tablero = new List<Piece>();
            for (int i = 0; i < 16; i++)
            {
                tablero.Add(null);
            }
            //var colorvalues = Enum.GetValues(typeof(Piece));
            for (int i = 0; i < 16; i++)
            {
                tablero.Add(null);
            }


        }

        public List<int> GetCasillasVacias()
        {
            List<int> disponibles = new List<int>();

            for (int i = 0; i < tablero.Count; i++)
            {

                if (tablero[i] == null)
                {

                    disponibles.Add(i);
                }
            }
            return disponibles;

        }

        public void SetSuper(Board gameState)
        {
            this.notWinningPieces.Clear();
            this.notWinningPieces.AddRange(gameState.GetNotWinningPieces());//.clone();

            for (int i = 0; i < gameState.GetWinningPieceSorts().Length; i++)
            {
                this.winningPieceSorts[i] = gameState.GetWinningPieceSorts()[i];
            }

            for (int i = 0; i < gameState.GetPossibleWins().GetLength(0); i++)
            {
                for (int j = 0; j < gameState.GetPossibleWins().GetLength(1); j++)
                {
                    //this.possibleWins[i][j] = gameState.GetPossibleWins()[i][j];
                }
            }

            for (int i = 0; i < gameState.GetPieceCounts().Length; i++)
            {
                this.pieceCounts[i] = gameState.GetPieceCounts()[i];
            }
        }

        public List<Piece> GetNotWinningPieces()
        {
            return notWinningPieces;
        }

        public bool[] GetWinningPieceSorts()
        {
            return winningPieceSorts;
        }

        public bool[,] GetPossibleWins()
        {
            return possibleWins;
        }

        public int[] GetPieceCounts()
        {
            return pieceCounts;
        }

        private int GetValue(int depth, int initAlpha, int initBeta, bool checkForWin, bool heuristic)
        {
            //if (!isPlace)
            //{
            //    if (checkForWin && checkForWin())
            //    {
            //        return isMe ? 9999 : -9999;
            //    }
            //    else
            //    {
            //        if (superBoard)
            //        {
            //            if (((SuperBoard)gameState).getNotWinningPieces().size() == 0)
            //                return isMe ? -9998 : 9999;
            //            else if (depth == 0)
            //            {
            //                if (heuristic) return heuristicValueOfGameState();
            //                else return 0;
            //            }
            //            checkForWin = false;
            //        }
            //        else if (depth == 0)
            //        {
            //            return 0;
            //        }
            //    }
            //}
            //int alpha = initAlpha;
            //int beta = initBeta;
            //makeChildren();

            //if (isMe)
            //{
            //    int newDepth = depth;
            //    if (isPlace) newDepth--;
            //    for (SuperNode child : children)
            //    {
            //        alpha = Math.max(alpha, child.getValue(newDepth, alpha, beta, checkForWin, heuristic));
            //        if (beta <= alpha) break; // (Poda Beta)
            //    }
            //    return alpha;
            //}
            //else
            //{
            //    int newDepth = depth;
            //    if (isPlace) newDepth--;
            //    for (SuperNode child : children)
            //    {
            //        beta = Math.min(beta, child.getValue(newDepth, alpha, beta, checkForWin, heuristic));
            //        if (beta <= alpha) break; // (Poda Alfa)
            //    }
            //    return beta;
            //}
            return 1; //todo epineda 5/dic/2018
        }

        public List<Piece> GetColumna(int colNr)
        {
            List<Piece> col = new List<Piece>();

            for (int i = colNr; i <= colNr + 12; i += 4)
            {
                col.Add(tablero[i]);
            }

            return col;
        }

        public List<Piece> GetFila(int rowNr)
        {
            List<Piece> row = new List<Piece>();
            for (int i = rowNr * 4; i < (rowNr * 4) + 4; i++)
            {
                //row.add(this.board.get(i));
            }
            return row;
        }

    }
}
