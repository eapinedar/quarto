﻿using MongoDB.Bson;
using MongoDB.Driver;
using QuartoPoli.Business;
using QuartoPoli.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace QuartoPoli.DataLayer
{
    public class PartidaDA
    {
        private MongoClient _client = new MongoClient("mongodb://localhost:27017");
        public IMongoDatabase BaseDatos = null;
        private Partida objPartida;

        public PartidaDA()
        {
            BaseDatos = _client.GetDatabase("QuartoDB");
        }

        public async Task<Partida> ConsultarPartida(string idPartida)
        {
            try
            {
                ObjectId query_id = ObjectId.Parse(idPartida);
                // Buscar la partida
                //MongoConnection mongo = new MongoConnection();
                IMongoCollection<Partida> colPartida = this.BaseDatos.GetCollection<Partida>("Partida");
                var filter = Builders<Partida>.Filter.Eq(mm => mm._id, query_id);
                //var partidaDB = await colPartida.Find<Partida>(filter).FirstAsync();
                var partidaDB = await colPartida.Find<Partida>(filter).FirstOrDefaultAsync();

                return partidaDB;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<ViewModelPartida> AddPartida(string nombrePartida, string nombreUsuario)
        {
            var partidaVM = new ViewModelPartida { NombrePartida = nombrePartida, NombreUsuario = nombreUsuario };
            try
            {
                IMongoCollection<Partida> colPartida = this.BaseDatos.GetCollection<Partida>("Partida");
                partidaVM.IdJugador = Guid.NewGuid().ToString();
                Partida partida = new Partida
                {
                    NombrePartida = nombrePartida,
                    EstadoPartida = EstadosPartida.Creada,
                    FechaCreacion = DateTime.Now
                };

                partida.Jugadores.Add(new Jugador { Id = partidaVM.IdJugador, Nombre = nombreUsuario });

                await colPartida.InsertOneAsync(partida);
                partidaVM.IdPartida = partida.Id;
                partidaVM.Respuesta = new Resultado { Error = false, Mensaje = "OK" };
            }
            catch (Exception ex)
            {
                partidaVM.Respuesta = new Resultado { Error = true, Mensaje = ex.Message };
            }
            return partidaVM;
        }

        public async Task<ViewModelPartida> JoinPartida(string idPartida, string nombreUsuario)
        {
            var partidaVM = new ViewModelPartida { IdPartida = idPartida, NombreUsuario = nombreUsuario };

            try
            {
                ObjectId query_id = ObjectId.Parse(idPartida);
                // Buscar la partida
                IMongoCollection<Partida> colPartida = this.BaseDatos.GetCollection<Partida>("Partida");
                var filter = Builders<Partida>.Filter.Eq(mm => mm._id, query_id);
                Partida partidaDB = colPartida.Find<Partida>(filter).FirstOrDefault();
                // var partidaDB = await new PartidaDA().ConsultarPartida(idPartida);

                if (partidaDB != null)
                {
                    //verificar que tenga solo un usuario
                    if (partidaDB.Jugadores.Count == 1)
                    {
                        //Verificar  que no sea el mismo nombre del otro jugador
                        if (!partidaDB.Jugadores.FirstOrDefault().Nombre.Equals(nombreUsuario))
                        {
                            //Terminar de ingresar info de Objeto de respuesta
                            partidaVM.IdJugador = Guid.NewGuid().ToString();
                            partidaVM.NombrePartida = partidaDB.NombrePartida;
                            // Ingresar usuario solicitante a la partida
                            partidaDB.Jugadores.Add(new Jugador { Id = partidaVM.IdJugador, Nombre = nombreUsuario });

                            // realizar random  para saber que usuario comienza y responder
                            Random rnd = new Random(DateTime.Now.Millisecond);
                            int r = rnd.Next(partidaDB.Jugadores.Count);

                            //insertar usuario que responde en la tabla registro usuarios
                            partidaDB.IdUsuarioTurno = partidaDB.Jugadores[r].Id;

                            var update = Builders<Partida>.Update.Set(mm => mm.Jugadores, partidaDB.Jugadores)
                                                                    .Set(mm => mm.IdUsuarioTurno, partidaDB.IdUsuarioTurno)
                                                                        .Set(mm => mm.EstadoPartida, EstadosPartida.EnEsperaJugador)
                                                                        .Set(mm => mm.PendientePrimerMovimiento, true);
                            await colPartida.FindOneAndUpdateAsync(filter, update);
                            partidaVM.Respuesta = new Resultado { Error = false, Mensaje = "OK" };
                        }
                        else
                        {
                            partidaVM.Respuesta = new Resultado { Error = true, Mensaje = "Su oponente se llama igual!!!!" };
                        }
                    }
                    else
                    {
                        partidaVM.Respuesta = new Resultado { Error = true, Mensaje = "Partida no disponible!!!!" };
                    }
                }
                else
                {
                    partidaVM.Respuesta = new Resultado { Error = true, Mensaje = "Partida no existe o no disponible!!!!" };
                }
            }
            catch (Exception ex)
            {
                partidaVM.Respuesta = new Resultado { Error = true, Mensaje = ex.Message };
            }
            return partidaVM;

        }

        public async Task<Resultado> RealizarMovimiento(string idPartida, string idJugador, ViewModelFicha fichaentregar = null, int ubicaciontableroindicefila = -1, int ubicaciontableroindicecolumna = -1)
        {
            //var tableroVM = new ViewModelTablero();
            var respuesta = new Resultado();
            try
            {
                //var partidaDB = await new PartidaDA().ConsultarPartida(idPartida);
                ObjectId query_id = ObjectId.Parse(idPartida);
                // Buscar la partida
                IMongoCollection<Partida> colPartida = this.BaseDatos.GetCollection<Partida>("Partida");
                var filter = Builders<Partida>.Filter.Eq(mm => mm._id, query_id);
                objPartida = colPartida.Find<Partida>(filter).FirstOrDefault();

                if (objPartida != null)
                {
                    // Verifico que hayan fichas disponibles
                    if (objPartida.EstadoPartida == EstadosPartida.EnEsperaJugador)
                    {
                        var jugadorTurno = objPartida.Jugadores.Where(mm => mm.Id == idJugador).FirstOrDefault().Nombre;

                        //Realizo las respectivas validaciones del movimiento
                        //1. Cargo el estado actual de la partida y de las fichas en el validador
                        //var objTableroBO = new TableroBO(ref partidaDB);
                        //tableroActual = objPartida.Tablero.Casillas;
                        //fichasActual = objPartida.Tablero.Fichas;

                        //Verificar usuario que tiene que realizar jugada
                        if (objPartida.IdUsuarioTurno.Equals(idJugador))
                        {
                            //Es el primer movimiento, no es necesario validar las coordenadas ni Fichas
                            if (objPartida.PendientePrimerMovimiento)
                            {
                                var resultadoMovimiento = RegistrarMovimiento(idJugador, fichaentregar, ubicaciontableroindicefila, ubicaciontableroindicecolumna);

                                if (resultadoMovimiento.Error)
                                {
                                    //tableroVM.Respuesta = new Resultado { Error = true, Mensaje = resultadoMovimiento.Mensaje };
                                    respuesta = new Resultado { Error = true, Mensaje = resultadoMovimiento.Mensaje };
                                    if (resultadoMovimiento.ErrorMovimientoJugador)
                                    {
                                        await RegistrarPartidaPerdida(idJugador, colPartida, filter, resultadoMovimiento.Mensaje,
                                            fichaentregar, ubicaciontableroindicefila, ubicaciontableroindicecolumna);

                                    }

                                }
                                else
                                {
                                    var update = Builders<Partida>.Update.Set(mm => mm.Tablero.Fichas, objPartida.Tablero.Fichas)
                                                                    .Set(mm => mm.RegistroJugadas, objPartida.RegistroJugadas)
                                                                    .Set(mm => mm.PendientePrimerMovimiento, false)
                                                                    .Set(mm => mm.IdUsuarioTurno, objPartida.IdUsuarioTurno)//Cambio de turno
                                                                    .Set(mm => mm.FichaEnJuego, objPartida.FichaEnJuego);//Seteo nueva ficha a jugar (Para siguiente jugador)

                                    await colPartida.FindOneAndUpdateAsync(filter, update);

                                    //tableroVM.Fichas = objPartida.Tablero.Fichas;
                                    //tableroVM.Casillas = objPartida.Tablero.Casillas;
                                    respuesta = new Resultado { Error = false, Mensaje = "OK" };

                                }
                            }
                            else
                            {
                                //No es el primer movimiento

                                if (ubicaciontableroindicefila >= 0 && ubicaciontableroindicefila <= 3 && ubicaciontableroindicecolumna >= 0 && ubicaciontableroindicecolumna <= 3)
                                {
                                    //VERIFICACIONES
                                    // 1. Verificar si existe una ficha en la posición solicitada
                                    if (objPartida.Tablero.Casillas[ubicaciontableroindicefila, ubicaciontableroindicecolumna] != null)
                                    {
                                        respuesta = new Resultado { Error = true, Mensaje = "¡Posición no válida. Perdiste!" };
                                        //Pierde la partida. Gana el otro jugador
                                        await RegistrarPartidaPerdida(idJugador, colPartida,
                                                                        filter,
                                                                        string.Format("Error del usuario {0} - Ya existe una ficha en la posición solicitada", jugadorTurno),
                                                                         fichaentregar, ubicaciontableroindicefila, ubicaciontableroindicecolumna);
                                    }
                                    else
                                    {
                                        //Método encargado de realizar las validaciones adicionales de la partida
                                        var resultadoMovimiento = RegistrarMovimiento(idJugador, fichaentregar, ubicaciontableroindicefila, ubicaciontableroindicecolumna);

                                        if (resultadoMovimiento.Error)
                                        {
                                            //tableroVM.Respuesta = new Resultado { Error = true, Mensaje = resultadoMovimiento.Mensaje };
                                            respuesta = new Resultado { Error = true, Mensaje = resultadoMovimiento.Mensaje };
                                            if (resultadoMovimiento.ErrorMovimientoJugador)
                                            {
                                                await RegistrarPartidaPerdida(idJugador, colPartida, filter, resultadoMovimiento.Mensaje,
                                                    fichaentregar, ubicaciontableroindicefila, ubicaciontableroindicecolumna);

                                            }

                                        }
                                        else
                                        {
                                            var update = Builders<Partida>.Update.Set(mm => mm.Tablero.Fichas, objPartida.Tablero.Fichas)
                                                        .Set(mm => mm.Tablero.Casillas, objPartida.Tablero.Casillas)
                                                        .Set(mm => mm.RegistroJugadas, objPartida.RegistroJugadas)
                                                        .Set(mm => mm.IdUsuarioTurno, objPartida.IdUsuarioTurno)//Cambio de turno
                                                        .Set(mm => mm.FichaEnJuego, objPartida.FichaEnJuego);//Seteo nueva ficha a jugar (Para siguiente jugador)
                                            await colPartida.FindOneAndUpdateAsync(filter, update);

                                            respuesta = new Resultado { Error = false, Mensaje = "OK" };

                                            //tableroVM.Fichas = objPartida.Tablero.Fichas;
                                            //tableroVM.Casillas = objPartida.Tablero.Casillas;

                                            //Ya hubo ganador. Actualizar Estado partida (Finalizarla)
                                            if (objPartida.HuboGanador)
                                            {
                                                var updateganador = Builders<Partida>.Update.Set(mm => mm.HuboGanador, objPartida.HuboGanador)
                                                                                            .Set(mm => mm.MotivoVictoria, objPartida.MotivoVictoria)
                                                                                            .Set(mm => mm.IdUsuarioGanador, idJugador)
                                                                                            .Set(mm => mm.EstadoPartida, objPartida.EstadoPartida);

                                                //EPINEDA 11/oct/2018 Bug actualización ganador y estado partida
                                                await colPartida.FindOneAndUpdateAsync(filter, updateganador);
                                                respuesta = new Resultado { Error = true, Mensaje = "¡WINNER!" };
                                            }
                                            else
                                            {
                                                //Era la última movida posible y no hubo ganador
                                                if (objPartida.PendienteUltimoMovimiento)
                                                {
                                                    var updateganador = Builders<Partida>.Update.Set(mm => mm.HuboGanador, false)
                                                                                            .Set(mm => mm.EstadoPartida, EstadosPartida.Finalizada);

                                                    await colPartida.FindOneAndUpdateAsync(filter, update);
                                                    respuesta = new Resultado { Error = true, Mensaje = "¡HUBO EMPATE!" };
                                                }
                                                else
                                                {
                                                    //Verificar si ya se acabaron las fichas
                                                    //cORRECCIÓN 06/DIC/2018 Consultar fichas de la BD
                                                    //var hayfichas = objPartida.Tablero.Fichas.Any(mm => mm.Disponible == true);
                                                    var dbPartidaFichas = colPartida.Find<Partida>(filter).FirstOrDefault();
                                                    var hayfichas = dbPartidaFichas.Tablero.Fichas.Any(mm => mm.Disponible == true);


                                                    if (!hayfichas)
                                                    {
                                                        //Se repartieron todas las fichas, juego entra en  último movimiento
                                                        var updateganador = Builders<Partida>.Update.Set(mm => mm.PendienteUltimoMovimiento, true);
                                                        await colPartida.FindOneAndUpdateAsync(filter, update);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    respuesta = new Resultado { Error = true, Mensaje = "Coordenada [" + ubicaciontableroindicefila + "][" + ubicaciontableroindicecolumna + "] no válida!!!" };
                                    await RegistrarPartidaPerdida(idJugador, colPartida, filter, string.Format("Error del usuario {0} - Coordenada[{1}][{2}] no válida!!!", jugadorTurno,
                                        ubicaciontableroindicefila, ubicaciontableroindicecolumna),
                                        fichaentregar, ubicaciontableroindicefila, ubicaciontableroindicecolumna);
                                }
                            }
                        }
                        else
                        {
                            respuesta = new Resultado { Error = true, Mensaje = "No era su turno, apague y vámonos!!!!" };
                            await RegistrarPartidaPerdida(idJugador, colPartida, filter, string.Format("Error del usuario {0} - No era su turno!!!", jugadorTurno),
                                  fichaentregar, ubicaciontableroindicefila, ubicaciontableroindicecolumna);
                        }
                    }
                    else
                    {
                        respuesta = new Resultado { Error = true, Mensaje = "Partida ya se encuentra finalizada, o en espera de que se una adversario!!!!" };
                    }
                }
                else
                {
                    respuesta = new Resultado { Error = true, Mensaje = "Partida no existe!!!!" };
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return respuesta;
        }

        private async Task RegistrarPartidaPerdida(string idJugador, IMongoCollection<Partida> colPartida, FilterDefinition<Partida> filter, string causavictoria, ViewModelFicha fichaentregar = null, int ubicaciontableroindicefila = -1, int ubicaciontableroindicecolumna = -1)
        {

            var dbfichaentregar = objPartida.Tablero.Fichas.Where(mm => mm.Alta == fichaentregar.Alta && mm.Blanca == fichaentregar.Blanca
                                     && mm.Cuadrada == fichaentregar.Cuadrada && mm.Llena == fichaentregar.Llena).FirstOrDefault();

            objPartida.RegistroJugadas.Add(new Jugada
            {
                //EAPINEDAR Fix Bug Jugador Registro Jugada
                NombreJugador = objPartida.Jugadores.Where(mm => mm.Id == idJugador).FirstOrDefault().Nombre,
                FechaHora = DateTime.Now,
                FichaEntregada = string.Format("{0}, {1}, {2}, {3}",
                                                           dbfichaentregar.Blanca ? "Blanca" : "Negra",
                                                           dbfichaentregar.Alta ? "Alta" : "Baja",
                                                           dbfichaentregar.Cuadrada ? "Cuadrada" : "Redonda",
                                                           dbfichaentregar.Llena ? "Llena" : "Hueca"),
                PosicionJugadaFichaAnteriorIndiceFila = ubicaciontableroindicefila,
                PosicionJugadaFichaAnteriorIndiceColumna = ubicaciontableroindicecolumna
            });


            var update = Builders<Partida>.Update.Set(mm => mm.EstadoPartida, EstadosPartida.Finalizada)
                                                 .Set(mm => mm.HuboGanador, true)
                                                 .Set(mm => mm.RegistroJugadas, objPartida.RegistroJugadas)
                                                 .Set(mm => mm.IdUsuarioGanador, objPartida.Jugadores.Where(mm => mm.Id != idJugador).FirstOrDefault().Id)
                                                 .Set(mm => mm.MotivoVictoria, causavictoria);

            await colPartida.FindOneAndUpdateAsync(filter, update);
        }

        private Resultado RegistrarMovimiento(string idJugador, ViewModelFicha fichaentregar = null, int ubicaciontableroindicefila = -1, int ubicaciontableroindicecolumna = -1)
        {
            Ficha[,] tableroActual = objPartida.Tablero.Casillas;
            List<Ficha> fichasActual = objPartida.Tablero.Fichas;

            //Último movimiento no deben entregarme ni validar ficha....
            if (objPartida.PendienteUltimoMovimiento)
            {
                //2. Pongo ficha entregada en turno anterior
                tableroActual[ubicaciontableroindicefila, ubicaciontableroindicecolumna] = objPartida.FichaEnJuego;
                return new Resultado { Error = false, Mensaje = "OK" };
            }
            else
            {
                //2. Verificar si la ficha que van a entregar ya fue utilizada3
                var dbfichaentregar = fichasActual.Where(mm => mm.Alta == fichaentregar.Alta && mm.Blanca == fichaentregar.Blanca
                                        && mm.Cuadrada == fichaentregar.Cuadrada && mm.Llena == fichaentregar.Llena).FirstOrDefault();

                if (dbfichaentregar == null)
                {
                    return new Resultado { Error = true, Mensaje = string.Format("¡Ficha no encontrada!"), ErrorMovimientoJugador = true };
                }
                else
                {
                    if (dbfichaentregar.Disponible)
                    {
                        var nombreJugador = objPartida.Jugadores.Where(mm => mm.Id == idJugador).FirstOrDefault().Nombre;

                        //1. Actualizamos en base de datos el estado del tablero y las fichas disponibles
                        dbfichaentregar.Disponible = false;

                        if (!objPartida.PendientePrimerMovimiento)
                        {
                            //2. Pongo ficha entregada en turno anterior
                            tableroActual[ubicaciontableroindicefila, ubicaciontableroindicecolumna] = objPartida.FichaEnJuego;
                        }

                        ////Seteo nueva ficha a jugar (Para siguiente jugador)
                        objPartida.FichaEnJuego = dbfichaentregar;


                        //Hacemos cambio de turno
                        var jugadorturnoNuevo = objPartida.Jugadores.Where(mm => mm.Id != idJugador).FirstOrDefault();
                        objPartida.IdUsuarioTurno = jugadorturnoNuevo.Id;


                        //Registrar movimiento
                        objPartida.RegistroJugadas.Add(new Jugada
                        {
                            //EAPINEDAR Fix Bug Jugador Registro Jugada
                            NombreJugador = nombreJugador ,
                            FechaHora = DateTime.Now,
                            FichaEntregada = string.Format(" {0}, {1}, {2}, {3}",
                                                            dbfichaentregar.Blanca ? "Blanca" : "Negra",
                                                            dbfichaentregar.Alta ? "Alta" : "Baja",
                                                            dbfichaentregar.Cuadrada ? "Cuadrada" : "Redonda",
                                                            dbfichaentregar.Llena ? "Llena" : "Hueca"),
                            PosicionJugadaFichaAnteriorIndiceFila = ubicaciontableroindicefila,
                            PosicionJugadaFichaAnteriorIndiceColumna = ubicaciontableroindicecolumna
                        });

                        //Verificar Ganador
                        string coordenadasganador = string.Empty;
                        if (VerificarGanador(tableroActual, ref coordenadasganador))
                        {
                            objPartida.HuboGanador = true;
                            objPartida.EstadoPartida = EstadosPartida.Finalizada;
                            objPartida.MotivoVictoria = " Usuario " + nombreJugador + " Gana: " + coordenadasganador;
                        }
                        return new Resultado { Error = false, Mensaje = "OK" };
                    }
                    else
                    {
                        return new Resultado { Error = true, Mensaje = "¡Ficha ya se puso en el tablero!", ErrorMovimientoJugador = true };
                    }
                }
            }
        }

        public async Task<ViewModelEstadoPartida> VerificarTurnoJugador(string idPartida, string idjugador, bool espectador = false)
        {
            var estadoturnoVM = new ViewModelEstadoPartida { IdPartida = idPartida, TurnoJugador = false };

            try
            {
                var partidaDB = await new PartidaDA().ConsultarPartida(idPartida);

                if (partidaDB != null)
                {
                    if (espectador)
                    {
                        var usuarioturno = partidaDB.Jugadores.Where(mm => mm.Id == partidaDB.IdUsuarioTurno).FirstOrDefault();

                        if (usuarioturno != null)
                        {
                            estadoturnoVM.JugadorEnTurno = usuarioturno.Nombre;
                        }


                        estadoturnoVM.FichaEnJuego = partidaDB.FichaEnJuego;
                        estadoturnoVM.Tablero = new ViewModelTablero
                        {
                            Casillas = partidaDB.Tablero.Casillas,
                            Fichas = partidaDB.Tablero.Fichas
                        };

                        estadoturnoVM.Respuesta = new Resultado { Error = false, Mensaje = "OK" };
                    }
                    else
                    {

                        if (partidaDB.EstadoPartida == EstadosPartida.EnEsperaJugador)
                        {
                            //Verificar usuario que tiene que realizar jugada

                            if (partidaDB.IdUsuarioTurno.Equals(idjugador))
                            {
                                //Me toca jugar!
                                estadoturnoVM.TurnoJugador = true;
                                //estadoturnoVM.FichaEnJuego = partidaDB.Tablero.Fichas.Where(mm => mm.Id == partidaDB.FichaEnJuego).FirstOrDefault();
                                estadoturnoVM.FichaEnJuego = partidaDB.FichaEnJuego;
                                estadoturnoVM.Tablero = new ViewModelTablero
                                {
                                    Casillas = partidaDB.Tablero.Casillas,
                                    Fichas = partidaDB.Tablero.Fichas
                                };
                            }



                            estadoturnoVM.Respuesta = new Resultado { Error = false, Mensaje = "OK" };
                            //Retoranar nombre
                            // return jugadorturno.Nombre;
                        }
                        else if (partidaDB.EstadoPartida == EstadosPartida.Finalizada)
                        {
                            estadoturnoVM.PartidaFinalizada = true;
                            estadoturnoVM.Respuesta = new Resultado { Error = true, Mensaje = "Partida ya se encuentra finalizada!!!!" };
                        }
                        else
                        {
                            estadoturnoVM.Respuesta = new Resultado { Error = true, Mensaje = "Partida en espera de que se una adversario!!!!" };
                        }
                    }
                }
                else
                {
                    estadoturnoVM.Respuesta = new Resultado { Error = true, Mensaje = "Partida no existe!!!!" };
                }
            }
            catch (Exception ex)
            {
                estadoturnoVM.Respuesta = new Resultado { Error = true, Mensaje = ex.Message };
            }
            return estadoturnoVM;
        }

        //public async Task<int> UpdatePartida(Partida partida)
        //{
        //    int resultado = 0;
        //    try
        //    {
        //        IMongoCollection<Partida> colPartida = db.GetCollection<Partida>("Partida");
        //        await colPartida.InsertOneAsync(newPartida);
        //        resultado = newPartida.Id;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //    return resultado;
        //}

        private bool VerificarGanador(Ficha[,] tableroActual, ref string comogano)
        {
            bool quarto = false;
            //Verificar diagonal principal
            quarto = VerificadorPatrones(tableroActual[0, 0], tableroActual[1, 1], tableroActual[2, 2], tableroActual[3, 3]);
            if (quarto)
            {
                comogano = "Diagonal Principal";
                return quarto;
            }

            // Verificar diagonal secundaria
            quarto = VerificadorPatrones(tableroActual[0, 3], tableroActual[1, 2], tableroActual[2, 1], tableroActual[3, 0]);
            if (quarto)
            {
                comogano = "Diagonal secundaria";
                return quarto;
            }

            // Verificar las filas y las columnas
            for (int i = 0; i < tableroActual.GetLength(0); i++)
            {
                //Verificar filas
                //Ficha[] lineacompleta = tableroActual[i];
                quarto = VerificadorPatrones(tableroActual[i, 0], tableroActual[i, 1], tableroActual[i, 2], tableroActual[i, 3]);

                if (quarto)
                {
                    int fila = i + 1;
                    comogano = "Fila " + fila.ToString();
                    return quarto;
                }
                //Verificar las columnas
                Ficha[] resultadoxcolumna = new Ficha[4];

                for (int j = 0; j < 4; j++)
                {
                    resultadoxcolumna[j] = tableroActual[j, i];
                }
                quarto = VerificadorPatrones(resultadoxcolumna[0], resultadoxcolumna[1], resultadoxcolumna[2], resultadoxcolumna[3]);

                if (quarto)
                {
                    int columna = i + 1;

                    comogano = "Columna " + columna.ToString();
                    return quarto;
                }
            }
            return quarto;
        }

        private bool VerificadorPatrones(Ficha primera, Ficha segunda, Ficha tercera, Ficha quarto)
        {
            // Verificar si alguna Ficha está vacía para no hacer más validaciones         
            if (primera == null || segunda == null || tercera == null || quarto == null)
            {
                return false;
            }

            // Verificar si hay 4 fichas seguidas con alguna característica en común (Altura, Color, Forma, Borde Superior)
            if (primera.Alta == segunda.Alta && primera.Alta == tercera.Alta && primera.Alta == quarto.Alta)
            {
                return true;
            }

            else if (primera.Blanca == segunda.Blanca && primera.Blanca == tercera.Blanca && primera.Blanca == quarto.Blanca)
            {
                return true;
            }

            else if (primera.Llena == segunda.Llena && primera.Llena == tercera.Llena && primera.Llena == quarto.Llena)
            {
                return true;
            }

            else if (primera.Cuadrada == segunda.Cuadrada && primera.Cuadrada == tercera.Cuadrada && primera.Cuadrada == quarto.Cuadrada)
            {
                return true;
            }

            //Continuar el juego, no existe ganador.
            return false;
        }

        #region PartidasEnCurso
        public async Task<ViewModelPartidaGrafica> ConsultarEstadoPartida(string idPartida)
        {
            var partidaVM = new ViewModelPartidaGrafica { IdPartida = idPartida };

            try
            {
                var partidaDB = await new PartidaDA().ConsultarPartida(idPartida);
                Ficha[] arrCasillas = new Ficha[16];

                if (partidaDB != null)
                {
                    var usuarioturno = partidaDB.Jugadores.Where(mm => mm.Id == partidaDB.IdUsuarioTurno).FirstOrDefault();


                    if (usuarioturno != null)
                    {
                        partidaVM.JugadorEnTurno = usuarioturno.Nombre;
                    }
                    partidaVM.FichaEnJuego = partidaDB.FichaEnJuego;
                    partidaVM.NombrePartida = partidaDB.NombrePartida;
                    partidaVM.HuboGanador = partidaDB.HuboGanador;
                    if (partidaVM.HuboGanador)
                    {
                        partidaVM.JugadorGanador = partidaDB.Jugadores.Where(mm => mm.Id == partidaDB.IdUsuarioGanador).FirstOrDefault().Nombre + " - Con ID = " + partidaDB.IdUsuarioGanador;
                    }
                    if (partidaDB.EstadoPartida == EstadosPartida.Finalizada)
                    {
                        partidaVM.PartidaFinalizada = true;
                        partidaVM.MotivoVictoria = partidaDB.MotivoVictoria;
                    }
                    //Registro de movimientos
                    partidaVM.RegistroJugadas = partidaDB.RegistroJugadas;

                    //var infos = TimeZoneInfo.GetSystemTimeZones();
                    //foreach (var info in infos)
                    //{
                    //    Console.WriteLine(info.Id);
                    //}

                    foreach (var jugada in partidaVM.RegistroJugadas)
                    {
                        DateTimeOffset dtoUtc = new DateTimeOffset(jugada.FechaHora, TimeSpan.Zero);

                        TimeZoneInfo colombia = TimeZoneInfo.FindSystemTimeZoneById("SA Pacific Standard Time");
                        DateTimeOffset dtoLocal = TimeZoneInfo.ConvertTime(dtoUtc, colombia);

                        jugada.FechaHora = dtoLocal.DateTime;

                    }

                    //Conversión de matriz a arreglo
                    int n = 0;
                    for (int i = 0; i < partidaDB.Tablero.Casillas.GetLength(0); i++)
                    {
                        for (int j = 0; j < partidaDB.Tablero.Casillas.GetLength(1); j++)
                        {
                            arrCasillas[n] = partidaDB.Tablero.Casillas[i, j];
                            n++;
                        }
                    }
                    partidaVM.Tablero = new ViewModelTableroGrafico
                    {
                        Casillas = arrCasillas,
                        Fichas = partidaDB.Tablero.Fichas
                    };

                    partidaVM.Respuesta = new Resultado { Error = false, Mensaje = "OK" };
                }
                else
                {
                    partidaVM.Respuesta = new Resultado { Error = true, Mensaje = "Partida no existe!!!!" };
                }
            }
            catch (Exception ex)
            {
                partidaVM.Respuesta = new Resultado
                {
                    Error = true,
                    Mensaje = ex.Message
                };
            }
            return partidaVM;
        }


        public async Task<List<ViewModelPartidaEnCurso>> ObtenerPartidas()
        {
            try
            {
                //MongoConnection mongo = new MongoConnection();
                List<ViewModelPartidaEnCurso> lstPartida = new List<ViewModelPartidaEnCurso>();
                IMongoCollection<Partida> colPartida = this.BaseDatos.GetCollection<Partida>("Partida");
                var lstPartidasDB = await colPartida.Find(_ => true).ToListAsync();


                foreach (var itempartida in lstPartidasDB)
                {
                    var estadoPartida = string.Empty;
                    var jugadoruno = string.Empty;
                    var jugadordos = string.Empty;
                    DateTime fechacreacion = new DateTime();

                    if (itempartida.EstadoPartida == EstadosPartida.EnEsperaJugador)
                    {
                        var jugadorturno = itempartida.Jugadores.Where(mm => mm.Id == itempartida.IdUsuarioTurno).FirstOrDefault();

                        if (jugadorturno != null)
                        {
                            estadoPartida = "En espera del jugador: " + jugadorturno.Nombre;
                        }
                        else
                        {
                            estadoPartida = itempartida.EstadoPartida.ToString();
                        }
                    }
                    else
                    {
                        estadoPartida = itempartida.EstadoPartida.ToString();
                    }

                    if (itempartida.Jugadores[0] != null)
                    {
                        jugadoruno = string.Format("{0}__{1}", itempartida.Jugadores[0].Nombre, itempartida.Jugadores[0].Id.Substring(0, 10).PadRight(20, 'x'));
                    }
                    if (itempartida.Jugadores.Count == 2)
                    {
                        if (itempartida.Jugadores[1] != null)
                        {
                            jugadordos = string.Format("{0}__{1}", itempartida.Jugadores[1].Nombre, itempartida.Jugadores[1].Id.Substring(0, 10).PadRight(20, 'x'));
                        }
                    }

                    if (itempartida.FechaCreacion != null)
                    {
                        DateTimeOffset dtoUtc = new DateTimeOffset(itempartida.FechaCreacion.Value, TimeSpan.Zero);

                        TimeZoneInfo colombia = TimeZoneInfo.FindSystemTimeZoneById("SA Pacific Standard Time");
                        DateTimeOffset dtoLocal = TimeZoneInfo.ConvertTime(dtoUtc, colombia);

                        fechacreacion = dtoLocal.DateTime;
                    }

                    lstPartida.Add(new ViewModelPartidaEnCurso() { Id = itempartida.Id, NombrePartida = itempartida.NombrePartida, EstadoPartida = estadoPartida, Jugador1 = jugadoruno, Jugador2 = jugadordos,FechaCreacion = fechacreacion });
                }

                return lstPartida.OrderByDescending(mm => mm.FechaCreacion).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}