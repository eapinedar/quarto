﻿using QuartoPoli.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Business
{
    public class TableroBO
    {
        //Ficha[][] tableroactual = Tablero.GetInstance.Casillas; //Consultar Base de Datos
        //List<Ficha> fichasactual = Tablero.GetInstance.Fichas;
        private Partida _partida;
        private Ficha[,] tableroActual;
        private List<Ficha> fichasActual;



        public TableroBO(ref Partida partida)
        {
            this._partida = partida;
            this.tableroActual = partida.Tablero.Casillas;
            this.fichasActual = partida.Tablero.Fichas;
        }

        public string RealizarMovimiento(Ficha ficha, int ubicaciontableroposx, int ubicaciontableroposy)
        {
            string error = string.Empty;


            //VERIFICACIONESS
            // 1. Verificar si existe una ficha en la posición solicitada
            if (tableroActual[ubicaciontableroposx,ubicaciontableroposy] != null)
            {
                error = "¡Posición no válida!";
            }
            else
            {
                //2. Verificar si la ficha ya fue utilizada
                var fichautilizar = fichasActual.Where(mm => mm.Alta == ficha.Alta && mm.Blanca == ficha.Blanca && mm.Cuadrada == ficha.Cuadrada && mm.Llena == ficha.Llena).FirstOrDefault();

                if (fichautilizar == null)
                {
                    error = "Ficha no encontrada!";
                }
                else
                {
                    if (fichautilizar.Disponible)
                    {
                        tableroActual[ubicaciontableroposx,ubicaciontableroposy] = fichautilizar;
                        fichautilizar.Disponible = false;

                        //Verificar Ganador
                        if (VerificarGanador())
                        {
                            //ANEXAR GANADOR
                        }


                    }
                    else
                    {
                        error = "¡Ficha ya se puso en el tablero!";
                    }
                }
            }
            return error;
        }

        private bool VerificarGanador()
        {
            bool quarto = false;
            //Verificar diagonal principal
            quarto = VerificadorPatrones(tableroActual[0,0], tableroActual[1,1], tableroActual[2,2], tableroActual[3,3]);
            if (quarto)
            {
                return quarto;
            }

            // Verificar diagonal secundaria
            quarto = VerificadorPatrones(tableroActual[0,3], tableroActual[1,2], tableroActual[2,1], tableroActual[3,0]);
            if (quarto)
            {
                return quarto;
            }

            // Verificar las filas y las columnas
            for (int i = 0; i < tableroActual.Length; i++)
            {
                //Verificar filas
                //Ficha[] lineacompleta = tableroActual[i];
                quarto = VerificadorPatrones(tableroActual[i,0], tableroActual[i,1], tableroActual[i,2], tableroActual[i,3]);

                if (quarto)
                {
                    return quarto;
                }
                //Verificar las columnas
                Ficha[] resultadoxcolumna = new Ficha[4];
                for (int j = 0; j < 4; j++)
                {
                    resultadoxcolumna[j] = tableroActual[j,i];
                }
                quarto = VerificadorPatrones(resultadoxcolumna[0], resultadoxcolumna[1], resultadoxcolumna[2], resultadoxcolumna[3]);

                if (quarto)
                {
                    return quarto;
                }
            }
            return quarto;
        }

        private bool VerificadorPatrones(Ficha primera, Ficha segunda, Ficha tercera, Ficha quarto)
        {
            // Verificar si alguna Ficha está vacía para no hacer más validaciones         
            if (primera == null || segunda == null || tercera == null || quarto == null)
            {
                return false;
            }

            // Verificar si hay 4 fichas seguidas con alguna característica en común (Altura, Color, Forma, Borde Superior)
            if (primera.Alta == segunda.Alta && primera.Alta == tercera.Alta && primera.Alta == quarto.Alta)
            {
                return true;
            }

            else if (primera.Blanca == segunda.Blanca && primera.Blanca == tercera.Blanca && primera.Blanca == quarto.Blanca)
            {
                return true;
            }

            else if (primera.Llena == segunda.Llena && primera.Llena == tercera.Llena && primera.Llena == quarto.Llena)
            {
                return true;
            }

            else if (primera.Cuadrada == segunda.Cuadrada && primera.Cuadrada == tercera.Cuadrada && primera.Cuadrada == quarto.Cuadrada)
            {
                return true;
            }

            //Continuar el juego, no existe ganador.
            return false;
        }


    }
}