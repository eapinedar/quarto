﻿using Newtonsoft.Json.Linq;
using QuartoPoli.DataLayer;
using QuartoPoli.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace QuartoPoli.Controllers
{
    public class QuartoController : ApiController
    {

        //[HttpPost]
        //public async Task<bool> InsertFicha(Ficha ficha)
        //{
        //    Models.MongoConnection client = new MongoConnection();
        //    await client.AddFicha(ficha);
        //    return true;
        //}

        public int index()
        {
            return 1;
        }

        [HttpPost]
        public async Task<ViewModelPartida> CrearPartida(JObject form)
        {
            try
            {
                dynamic jsonobject = form;
                string nombrePartida = string.Empty;
                string nombreUsuario = string.Empty;

                PartidaDA client = new PartidaDA();

                try
                {
                    nombrePartida = jsonobject.nombrepartida.Value;
                    nombreUsuario = jsonobject.nombreusuario.Value;
                }
                catch (Exception ex)
                {
                    //return BadRequest("Falta Parámetro");
                    return new ViewModelPartida { Respuesta = new Resultado { Error = true, Mensaje = "Falta Parámetro" } };
                }

                ViewModelPartida partida = await client.AddPartida(nombrePartida, nombreUsuario);

                //Objeto de respuesta
                return partida;
            }
            catch (Exception ex)
            {
                return new ViewModelPartida { Respuesta = new Resultado { Error = true, Mensaje = ex.Message } };
            }
        }

        [HttpPost]
        public async Task<ViewModelPartida> UnirsePartida(JObject form)
        {
            try
            {
                string idPartida = string.Empty;
                string nombreUsuario = string.Empty;
                dynamic jsonobject = form;

                PartidaDA client = new PartidaDA();

                try
                {
                    idPartida = jsonobject.idpartida.Value;
                    nombreUsuario = jsonobject.nombreusuario.Value;
                }
                catch (Exception ex)
                {
                    //return BadRequest("Falta Parámetro");
                    return new ViewModelPartida { Respuesta = new Resultado { Error = true, Mensaje = "Falta Parámetro" } };
                }


                ViewModelPartida modelpartida = await client.JoinPartida(idPartida, nombreUsuario);
                return modelpartida;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<ViewModelEstadoPartida> VerificarEstadoPartida(string idPartida, string idJugador)
        {
            try
            {
                PartidaDA client = new PartidaDA();

                //Consultar partida, y usuario que esta en propiedad IdUsuarioTurno (Jugador que debe realizar movimiento)
                //var nombreusuarioturno = await client.VerificarPartida(idPartida, idjugador);
                ViewModelEstadoPartida modelestadopartida = await client.VerificarTurnoJugador(idPartida, idJugador);
                return modelestadopartida;
                //resultado.Error = false;
                //resultado.Mensaje = "Partida " + idPartida + " En espera de jugador: " + nombreusuarioturno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<ViewModelPartidaGrafica> ConsultarEstadoPartida(string idPartida)
        {
            try
            {
                PartidaDA client = new PartidaDA();

                //Consultar partida, y usuario que esta en propiedad IdUsuarioTurno (Jugador que debe realizar movimiento)
                //var nombreusuarioturno = await client.VerificarPartida(idPartida, idjugador);
                ViewModelPartidaGrafica modelestadopartida = await client.ConsultarEstadoPartida(idPartida);
                return modelestadopartida;
                //resultado.Error = false;
                //resultado.Mensaje = "Partida " + idPartida + " En espera de jugador: " + nombreusuarioturno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/Quarto/5
        [ResponseType(typeof(Tablero))]
        public async Task<Resultado> RealizarMovimiento(ViewModelMovimiento movimiento)
        {
            try
            {
                PartidaDA client = new PartidaDA();
                Resultado resultado = await client.RealizarMovimiento(movimiento.IdPartida,
                                                                      movimiento.IdJugador,
                                                                      movimiento.FichaEntregar,
                                                                      movimiento.Ubicaciontableroindicefila,
                                                                      movimiento.Ubicaciontableroindicecolumna);
                return resultado;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<ViewModelPartidaEnCurso>> GetPartidas()
        {
            try
            {
                PartidaDA client = new PartidaDA();
                var partidas = await client.ObtenerPartidas();
                return partidas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
