﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class ViewModelPartidaEnCurso
    {
        public string Id { get; set; }
        public string NombrePartida { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string Jugador1 { get; set; }
        public string Jugador2 { get; set; }
        public string EstadoPartida { get; set; }
    }
}