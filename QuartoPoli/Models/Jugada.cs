﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class Jugada
    {
        public int Id { get; set; }
        public DateTime FechaHora { get; set; }
        public string NombreJugador { get; set; }
        public int PosicionJugadaFichaAnteriorIndiceFila { get; set; }
        public int PosicionJugadaFichaAnteriorIndiceColumna { get; set; }
        public string FichaEntregada { get; set; }
    }
}