﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class Jugador
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        // Solo lo conoce el juego y el usuario en cuestion
        //public string Url { get; set; }
    }
}