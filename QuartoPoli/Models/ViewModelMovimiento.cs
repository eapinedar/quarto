﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class ViewModelMovimiento
    {
        private int _ubicaciontableroindicefila = -1;
        private int _ubicaciontableroindicecolumna = -1;

        public ViewModelFicha FichaEntregar { get; set; }
        public string IdPartida { get; set; }
        public string IdJugador { get; set; }
        public int Ubicaciontableroindicefila
        {
            get
            {
                return _ubicaciontableroindicefila;
            }
            set
            {
                _ubicaciontableroindicefila = value;
            }
        }
        public int Ubicaciontableroindicecolumna
        {
            get
            {
                return _ubicaciontableroindicecolumna;
            }
            set
            {
                _ubicaciontableroindicecolumna = value;
            }
        }
    }
}