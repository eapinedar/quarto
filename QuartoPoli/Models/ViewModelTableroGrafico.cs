﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class ViewModelTableroGrafico
    {
        public List<Ficha> Fichas { get; set; }
        //Casillas del Juego 4X4
        public Ficha[] Casillas { get; set; }
    }
}