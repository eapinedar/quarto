﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class ViewModelFicha
    {
        public bool Blanca { get; set; }
        public bool Alta { get; set; }
        public bool Cuadrada { get; set; }
        public bool Llena { get; set; }
    }
}