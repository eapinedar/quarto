﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class ViewModelPartidaGrafica
    {
        public string IdPartida { get; set; }
        public string JugadorEnTurno { get; set; }
        public string NombrePartida { get; set; }
        public Resultado Respuesta { get; set; }
        public Ficha FichaEnJuego { get; set; }
        public bool PartidaFinalizada { get; set; }
        public bool HuboGanador { get; set; }
        public string JugadorGanador { get; set; }
        public string MotivoVictoria { get; set; }
        public List<Jugada> RegistroJugadas { get; set; }

        public ViewModelTableroGrafico Tablero { get; set; }
    }
}