﻿namespace QuartoPoli.Models
{
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Collections.Generic;

    public class Partida
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonIgnore]
        public string Id { get { return this._id.ToString(); } }
        public string NombrePartida { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public EstadosPartida EstadoPartida { get; set; }
        public bool HuboGanador { get; set; }
        public bool PendientePrimerMovimiento { get; set; }
        public bool PendienteUltimoMovimiento { get; set; }
        public string IdUsuarioTurno { get; set; }
        public string IdUsuarioGanador { get; set; }
        public string MotivoVictoria { get; set; }

        public Ficha FichaEnJuego { get; set; }


        public Tablero Tablero { get; set; }
        public List<Jugador> Jugadores { get; set; }
        public List<Jugada> RegistroJugadas { get; set; }


        public Partida()
        {
            Tablero = new Tablero();
            Jugadores = new List<Jugador>();
            RegistroJugadas = new List<Jugada>();
        }
    }

    public enum EstadosPartida
    {
        Creada = 1,//Creada por jugador 1. En espera de que jugador 2 se una
        //EnEsperaPrimerMovimiento = 2, //Se unio Jugador 2. En espera del primer movimiento.
        EnEsperaJugador = 2,//A la espera de que el siguiente jugador realice movimiento
        Finalizada = 3//Partida completada
    }

}