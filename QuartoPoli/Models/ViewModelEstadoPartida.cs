﻿namespace QuartoPoli.Models
{
    public class ViewModelEstadoPartida
    {
        public string IdPartida { get; set; }
        public bool TurnoJugador { get; set; }
        public Ficha FichaEnJuego { get; set; }
        public Resultado Respuesta { get; set; }
        public ViewModelTablero Tablero { get; set; }
        public string JugadorEnTurno { get; set; }
        public bool PartidaFinalizada { get; set; }
    }
}