﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class Tablero
    {
        //16 Fichas del Juego
        public List<Ficha> Fichas { get; set; }

        //Casillas del Juego 4X4
        //public Ficha[][] Casillas { get; set; }
        public Ficha[,] Casillas { get; set; }

        public Tablero()
        {
            //Limpiamos tablero
            InicializarTablero();
        }

        private void InicializarTablero()
        {
            //Inicializamos todos los tipos de ficha posibles
            //this.Fichas = new Ficha[]
            //{
            //    new Ficha { Alta = false, Blanca = false, Cuadrada = false, Llana = false},//0000
            //    new Ficha { Alta = false, Blanca = false, Cuadrada = false, Llana = true},//0001
            //    new Ficha { Alta = false, Blanca = false, Cuadrada = true, Llana = false},//0010
            //    new Ficha { Alta = false, Blanca = false, Cuadrada = true, Llana = true},//0011
            //    new Ficha { Alta = false, Blanca = true, Cuadrada = false, Llana = false},//0100
            //    new Ficha { Alta = false, Blanca = true, Cuadrada = false, Llana = true},//0101
            //    new Ficha { Alta = false, Blanca = true, Cuadrada = true, Llana = false},//0110
            //    new Ficha { Alta = false, Blanca = true, Cuadrada = true, Llana = true},//0111
            //    new Ficha { Alta = true, Blanca = false, Cuadrada = false, Llana = false},//1000
            //    new Ficha { Alta = true, Blanca = false, Cuadrada = false, Llana = true},//1001
            //    new Ficha { Alta = true, Blanca = false, Cuadrada = true, Llana = false},//1010
            //    new Ficha { Alta = true, Blanca = false, Cuadrada = true, Llana = true},//1011
            //    new Ficha { Alta = true, Blanca = true, Cuadrada = false, Llana = false},//1100
            //    new Ficha { Alta = true, Blanca = true, Cuadrada = false, Llana = true},//1101
            //    new Ficha { Alta = true, Blanca = true, Cuadrada = true, Llana = false},//1110
            //    new Ficha { Alta = true, Blanca = true, Cuadrada = true, Llana = true},//1111
            //};
            //Inicializamos todos los tipos de ficha posibles
            // Crear Fichas
            this.Fichas = new List<Ficha>
            {
                new Ficha { Id = 1, Alta = false, Blanca = false, Cuadrada = false, Llena = false, Disponible = true },//0000)
                new Ficha { Id = 2, Alta = false, Blanca = false, Cuadrada = false, Llena = true, Disponible = true },//0001
                new Ficha { Id = 3, Alta = false, Blanca = false, Cuadrada = true, Llena = false, Disponible = true },//0010
                new Ficha { Id = 4, Alta = false, Blanca = false, Cuadrada = true, Llena = true, Disponible = true },//0011
                new Ficha { Id = 5, Alta = false, Blanca = true, Cuadrada = false, Llena = false, Disponible = true },//0100
                new Ficha { Id = 6, Alta = false, Blanca = true, Cuadrada = false, Llena = true, Disponible = true },//0101
                new Ficha { Id = 7, Alta = false, Blanca = true, Cuadrada = true, Llena = false, Disponible = true },//0110
                new Ficha { Id = 8, Alta = false, Blanca = true, Cuadrada = true, Llena = true, Disponible = true },//0111
                new Ficha { Id = 9, Alta = true, Blanca = false, Cuadrada = false, Llena = false, Disponible = true },//1000
                new Ficha { Id = 10, Alta = true, Blanca = false, Cuadrada = false, Llena = true, Disponible = true },//1001
                new Ficha { Id = 11, Alta = true, Blanca = false, Cuadrada = true, Llena = false, Disponible = true },//1010
                new Ficha { Id = 12, Alta = true, Blanca = false, Cuadrada = true, Llena = true, Disponible = true },//1011
                new Ficha { Id = 13, Alta = true, Blanca = true, Cuadrada = false, Llena = false, Disponible = true },//1100
                new Ficha { Id = 14, Alta = true, Blanca = true, Cuadrada = false, Llena = true, Disponible = true },//1101
                new Ficha { Id = 15, Alta = true, Blanca = true, Cuadrada = true, Llena = false, Disponible = true },//1110
                new Ficha { Id = 16, Alta = true, Blanca = true, Cuadrada = true, Llena = true, Disponible = true }//1111
            };


            //Inicializamos las fichas del tablero vacías(Sin fichas asignadas)
            // Crear Tablero
            //this.Casillas = new Ficha[4][4]{
            //        new Ficha[] { null, null, null, null },
            //        new Ficha[] { null, null, null, null },
            //        new Ficha[] { null, null, null, null },
            //        new Ficha[] { null, null, null, null }
            //};

            this.Casillas = new Ficha[4, 4]{
                  { null,null,null,null},
                  { null,null,null,null},
                  { null,null,null,null},
                  { null,null,null,null},
            };

        }
    }
}