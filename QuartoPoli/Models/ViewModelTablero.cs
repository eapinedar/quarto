﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class ViewModelTablero
    {
        //16 Fichas del Juego
        public List<Ficha> Fichas { get; set; }

        //Casillas del Juego 4X4
        public Ficha[,] Casillas { get; set; }

        //public Resultado Respuesta { get; set; }

    }
}