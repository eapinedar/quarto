﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuartoPoli.Models
{
    public class ViewModelPartida
    {
        public string IdPartida { get; set; }
        public string NombrePartida { get; set; }
        public string IdJugador { get; set; }
        public string NombreUsuario { get; set; }
        public Resultado Respuesta { get; set; }
    }
}