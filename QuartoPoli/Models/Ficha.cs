﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace QuartoPoli.Models
{
    public class Ficha
    {
        //[BsonId]
        //public BsonDocument _Id { get; set; }
        public int Id { get; set; }
        public bool Blanca { get; set; }
        public bool Alta { get; set; }
        public bool Cuadrada { get; set; }
        public bool Llena { get; set; }

        public bool Disponible { get; set; }

        //public bool[] Identificador
        //{
        //    get
        //    {
        //        return new bool[] { Blanca, Alta, Cuadrada, Llana };
        //    }
        //}
    }
}
