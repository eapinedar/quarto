﻿namespace QuartoPoli.Models
{
    public class Resultado
    {
        public bool Error { get; set; }
        public bool ErrorMovimientoJugador { get; set; }
        public string Mensaje { get; set; }
    }
}