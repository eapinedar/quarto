﻿namespace Quarto.Domain
{
    public class ViewModelEstadoPartida
    {
        public string IdPartida { get; set; }
        public bool TurnoJugador { get; set; }
        public Ficha FichaEnJuego { get; set; }
        public bool PartidaFinalizada { get; set; }
        public Resultado Respuesta { get; set; }
        public ViewModelTablero Tablero { get; set; }

    }
}
