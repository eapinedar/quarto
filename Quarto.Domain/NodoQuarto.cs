﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quarto.Domain
{
    public class NodoQuarto
    {
        public int casilla;
        public int fichapasada;
        public char jugador;
        public List<NodoQuarto> nodoshijo;
        public bool esHoja;
        public bool esQuarto;

        public NodoQuarto(int casilla, int fichapasada, char jugador)
        {
            this.casilla = casilla;
            this.fichapasada = fichapasada;
            this.jugador = jugador;
            nodoshijo = new List<NodoQuarto>();
            esHoja = true;
            esQuarto = false;
        }

        public bool AgregarHijo(NodoQuarto nodo)
        {
            nodoshijo.Add(nodo);
            this.esHoja = false;
            return true;

        }

        /// <summary>
        /// Imprime en Consola la información del Nodo (Casilla, Ficha, Jugador(Máximo o mínimo), hay Quarto en este nodo)
        /// </summary>
        /// <returns></returns>
        public string CadenaArbol()
        {
            String str = "<" + casilla + "," + fichapasada + "," + jugador;
            if (esQuarto)
                str += ",Q!";
            str += ">";
            return str;
        }
    }
}
