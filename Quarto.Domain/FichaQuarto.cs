﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quarto.Domain
{
    public class FichaQuarto
    {
        /// <summary>
        /// Método que recibe 4 casillas como entrada y se encarga de verificar si tienen alguna propiedad en común... Es decir si Hubo Quarto
        /// </summary>
        /// <param name="casilla1"></param>
        /// <param name="casilla2"></param>
        /// <param name="casilla3"></param>
        /// <param name="casilla4"></param>
        /// <returns></returns>
        public static int CompararFichas(int casilla1, int casilla2, int casilla3, int casilla4)
        {
            // Retorna los atributos que hicieron Quarto
            int coincidencias = 0;

            //Se debe recordar que existen 16 fichas: 
            //   =  A B C L   => Alta? , Blanca, Cuadrada?, LLena?
            //0  =  0 0 0 0 
            //1  =  0 0 0 1
            //2  =  0 0 1 0
            //3  =  0 0 1 1
            //4  =  0 1 0 0
            //5  =  0 1 0 1
            //6  =  0 1 1 0
            //7  =  0 1 1 1
            //8  =  1 0 0 0
            //9  =  1 0 0 1
            //10 =  1 0 1 0
            //11 =  1 0 1 1
            //12 =  1 1 0 0
            //13 =  1 1 0 1
            //14 =  1 1 1 0
            //15 =  1 1 1 1
            //Por lo tanto se usa los operadores de desplazamiento a la derecha y desplazamiento a la izquierda para encontrar coincidencias a nivel de Bits

            for (int i = 0; i < 4; i++)
            {
                coincidencias = ((casilla1 & (1 << i)) >> i) + ((casilla2 & (1 << i)) >> i) + ((casilla3 & (1 << i)) >> i) + ((casilla4 & (1 << i)) >> i);
                if (coincidencias == 4 || coincidencias == 0)
                {
                    return i;
                }
                coincidencias = 0;
            }

            return 4;
        }
    }
}
