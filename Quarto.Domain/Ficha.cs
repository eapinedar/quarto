﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quarto.Domain
{
    public class Ficha
    {
        public int Id { get; set; }
        public bool Blanca { get; set; }
        public bool Alta { get; set; }
        public bool Cuadrada { get; set; }
        public bool Llena { get; set; }

        public bool Disponible { get; set; }

        public int IdentificadorFicha
        {
            get
            {
                int ialta = Alta ? 1 : 0;
                int iblanca = Blanca ? 1 : 0;
                int icuadrada = Cuadrada ? 1 : 0;
                int illena = Llena ? 1 : 0;

              return ialta * 8 + iblanca * 4 + icuadrada * 2 + illena;
            }
        }
    }
}
