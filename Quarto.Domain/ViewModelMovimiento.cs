﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quarto.Domain
{
    public class ViewModelMovimiento
    {
        public ViewModelFicha FichaEntregar { get; set; }
        public string IdPartida { get; set; }
        public string IdJugador { get; set; }
        public int Ubicaciontableroindicefila { get; set; }
        public int Ubicaciontableroindicecolumna { get; set; }
    }
}
