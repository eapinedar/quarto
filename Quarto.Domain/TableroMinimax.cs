﻿using System;

namespace Quarto.Domain
{
    [Serializable]
    public class TableroMinimax
    {
        public int[] estadotablero = {  16, 16, 16, 16,
                                        16, 16, 16, 16,
                                        16, 16, 16, 16,
                                        16, 16, 16, 16};

        public int[] estadoficha = new int[16];
        public int NumeroCasillasVacias { get; set; }
        public int NumeroFichasDisponibles { get; set; }
        public int fichapasada = 16;

        public static int FUERADELTABLERO = 0, FICHAENJUEGO = 1, FICHAPUESTA = 2;   // códigos para el estado de la ficha

        /// <summary>
        /// Entrega la ficha enviada como parámetro al método 
        /// </summary>
        /// <param name="casillavacia"></param>
        /// <returns></returns>
        public bool EntregarFicha(int ficha)
        {
            // sets the passed piece

            if (ficha < 0 || ficha > 15)
            {
                //Console.WriteLine("Ficha no existe");
                return false;
            }

            if (estadoficha[ficha] == FUERADELTABLERO)
            {
                fichapasada = ficha;
                estadoficha[ficha] = FICHAENJUEGO;
                NumeroFichasDisponibles--;

                return true;
            }
            else
            {
                Console.Write("La ficha está en juego o ya se encuentra en el tablero.");
                return false;
            }
        }

        /// <summary>
        /// Pone la ficha pasada en el tablero en la casilla vacía enviada como parámetro al método 
        /// </summary>
        /// <param name="casillavacia"></param>
        /// <returns></returns>
        public bool JugarFichaPasada(int casillavacia)
        {
            //Verificamos que sea una casilla válida
            if (casillavacia < 16 && casillavacia >= 0)
            {
                if (estadotablero[casillavacia] != 16)
                {
                    // Casilla no se encuentra vacia
                    Console.WriteLine("La casilla " + casillavacia + " no está vacia, tiene la ficha: " + estadotablero[casillavacia] + ").");
                    return false;
                }
                else if (estadoficha[fichapasada] == FUERADELTABLERO)
                {
                    // La ficha pasada no está disponible
                    Console.WriteLine("Error: El estado de la ficha " + fichapasada + " es " + ((estadoficha[fichapasada] == FUERADELTABLERO) ? "Fuera del tablero" : "Puesta") + ".");
                    return false;
                }
                else
                {
                    //Realizamos la simulación de la jugada
                    estadotablero[casillavacia] = fichapasada;
                    estadoficha[fichapasada] = FICHAPUESTA;
                    NumeroCasillasVacias--;
                    fichapasada = 16;
                    return true;
                }
            }
            else
            {
                Console.WriteLine("Casilla errónea: " + casillavacia);
                return false;
            }
        }

        /// <summary>
        /// Método encargado de verificar si hubo Quarto con la 
        /// </summary>
        /// <param name="casillavacia"></param>
        /// <returns></returns>
        public bool VerificarQuarto(int casillavacia)
        {
            //Variables auxiliares encargadas de validar si no hay alguna casilla en la fila o columna donde se simula la jugada con el fin de no validar quarto, o en las
            // diagonal principal o secundaria
            bool verificarFila = true;
            bool verificarColumna = true;
            bool verificarDiagonalPrincipal = true;
            bool verificarDiagonalSecundaria = true;

            int filadondesepuso = casillavacia - (casillavacia % 4);
            int columnadondesepuso = casillavacia % 4;

            int posicionfila1 = estadotablero[filadondesepuso];
            int posicionfila2 = estadotablero[filadondesepuso + 1];
            int posicionfila3 = estadotablero[filadondesepuso + 2];
            int posicionfila4 = estadotablero[filadondesepuso + 3];

            int posicioncolumna1 = estadotablero[columnadondesepuso];
            int posicioncolumna2 = estadotablero[columnadondesepuso + 4];
            int posicioncolumna3 = estadotablero[columnadondesepuso + 8];
            int posicioncolumna4 = estadotablero[columnadondesepuso + 12];

            // Verificar casillas vacias en la fila o la columna
            if (posicionfila1 == 16 || posicionfila2 == 16 ||
                 posicionfila3 == 16 || posicionfila4 == 16)
            {
                verificarFila = false;
            }
            if (posicioncolumna1 == 16 || posicioncolumna2 == 16 ||
                 posicioncolumna3 == 16 || posicioncolumna4 == 16)
            {
                verificarColumna = false;
            }
            //Verificar casillas vacías en las diagonales principales y secundarias
            if (estadotablero[0] == 16 || estadotablero[5] == 16 ||
                estadotablero[10] == 16 || estadotablero[15] == 16)
            {
                verificarDiagonalPrincipal = false;
            }

            if (estadotablero[3] == 16 || estadotablero[6] == 16 ||
                estadotablero[9] == 16 || estadotablero[12] == 16)
            {
                verificarDiagonalSecundaria = false;
            }

            if (!verificarFila && !verificarColumna && !verificarDiagonalPrincipal && !verificarDiagonalSecundaria)
            {
                return false;
            }
            // No existen casillas vacías empezar a comparar las fichas por diagonales fila y columna
            int huboquarto;

            // Verificar filas
            if (verificarFila)
            {
                huboquarto = FichaQuarto.CompararFichas(posicionfila1, posicionfila2, posicionfila3, posicionfila4);
                if (huboquarto < 4)
                    return true;
            }

            // Verificar Columnas
            if (verificarColumna)
            {
                huboquarto = FichaQuarto.CompararFichas(posicioncolumna1, posicioncolumna2, posicioncolumna3, posicioncolumna4);
                if (huboquarto < 4)
                    return true;
            }

            // Verificar Diagonal Principal
            if (verificarDiagonalPrincipal)
            {
                huboquarto = FichaQuarto.CompararFichas(estadotablero[0], estadotablero[5], estadotablero[10], estadotablero[15]);
                if (huboquarto < 4)
                    return true;
            }

            //Verificar diagonal secundaria
            if (verificarDiagonalSecundaria)
            {
                huboquarto = FichaQuarto.CompararFichas(estadotablero[3], estadotablero[6], estadotablero[9], estadotablero[12]);
                if (huboquarto < 4)
                    return true;
            }

            // No hay Quarto
            return false;
        }

        /// <summary>
        /// Método encargado de retornar las casillas vacías del tablero.
        /// </summary>
        /// <returns>Estructura de datos: Arreglo de enteros</returns>
        public int[] ObtenerCasillasVacias()
        {
            int[] arrcasillasvacias = new int[NumeroCasillasVacias];

            int m = 0;
            //Se recorre cada una de las 16 casillas del tablero.
            for (int i = 0; i < 16; i++)
            {
                if (estadotablero[i] == 16)
                {
                    arrcasillasvacias[m] = i;
                    m++;
                }
            }

            return arrcasillasvacias;
        }

        /// <summary>
        /// Método encargado de retornar las fichas que no han sido jugadas aún
        /// </summary>
        /// <returns>Estructura de datos: Arreglo de enteros</returns>
        public int[] ObtenerFichasDisponibles()
        {
            int[] fichasdisponibles = new int[NumeroFichasDisponibles];

            int m = 0;
            for (int i = 0; i < 16; i++)
            {
                if (estadoficha[i] == FUERADELTABLERO)
                {
                    fichasdisponibles[m] = i;
                    m++;
                }
            }

            return fichasdisponibles;
        }
    }
}
