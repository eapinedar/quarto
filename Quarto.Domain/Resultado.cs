﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quarto.Domain
{
    public class Resultado
    {
        public bool Error { get; set; }
        public string Mensaje { get; set; }
    }
}
