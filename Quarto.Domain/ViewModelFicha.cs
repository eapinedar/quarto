﻿namespace Quarto.Domain
{
    public class ViewModelFicha
    {
        public bool Alta { get; set; }
        public bool Blanca { get; set; }
        public bool Cuadrada { get; set; }
        public bool Llena { get; set; }
    }
}
