﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quarto.Domain
{
    public class ViewModelPartida
    {
        public string IdPartida { get; set; }
        public string NombrePartida { get; set; }
        public string IdJugador { get; set; }
        public string NombreUsuario { get; set; }
        public Resultado Respuesta { get; set; }
    }
}
