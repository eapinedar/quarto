﻿using System.Collections.Generic;

namespace Quarto.Domain
{
    public class ViewModelTablero
    {
        //16 Fichas del Juego
        public List<Ficha> Fichas { get; set; }

        //Casillas del Juego 4X4
        public Ficha[,] Casillas { get; set; }
    }
}
