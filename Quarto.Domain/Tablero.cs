﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quarto.Domain
{
    public class Tablero : ICloneable
    {
        public int nCasillasVacias;
        public int nFichasSinJugar;
        private int _fichapasada = 16;

        public static readonly int OFF_BOARD = 0, IN_LIMBO = 1, IN_PLAY = 2;   // códigos para el estado de la ficha


        //16 Fichas del Juego
        public List<Ficha> Fichas { get; set; }

        //Casillas del Juego 4X4
        public Ficha[,] Casillas { get; set; }


        public Tablero(Ficha[,] Casillas, List<Ficha> Fichas)
        {
            this.Casillas = Casillas;
            this.Fichas = Fichas;
            this.nCasillasVacias = GetNumeroCasillasVacias();
            this.nFichasSinJugar = GetNumeroFichasSinJugar();
        }

        public int GetNumeroCasillasVacias()
        {
            int filasvacias = 0;

            for (int col = 0; col < Casillas.GetLength(0); col++)
            {
                for (int row = 0; row < Casillas.GetLength(1); row++)
                {
                    if (Casillas[row, col] == null)
                    {
                        filasvacias++;
                    }
                }
            }
            return filasvacias;
        }

        public int GetNumeroFichasSinJugar()
        {
            return Fichas.Where(mm => mm.Disponible == true).Count();
        }

        public int[] GetArregloCasillasVacias()
        {
            int[] arrcasillasvacias = new int[nCasillasVacias];

            int j = 0;
            for (int row = 0; row < Casillas.GetLength(1); row++)
            {
                for (int col = 0; col < Casillas.GetLength(0); col++)
                {

                    if (Casillas[row,col] == null)
                    {
                        //EAPINEDAR 12/OCT/2018 Convertir coordenada [a,b] en posición matriz
                        int posind = 4 * row + col;
                        arrcasillasvacias[j] = posind;
                        j++;
                    }
                }
            }

            return arrcasillasvacias;
        }

        public int[] GetArregloFichasSinJugar()
        {
            int[] arrfichassinjugar = new int[nFichasSinJugar];

            int j = 0;
            for (int i = 0; i < 16; i++)
            {
                if (Fichas[i].Disponible)
                {
                    arrfichassinjugar[j] = i;
                    j++;
                }
            }

            return arrfichassinjugar;
        }

        public int GetFichaPasada()
        {
            return _fichapasada;
        }

        //public bool Jugar(int casilla)
        //{
        //    if (casilla < 16 && casilla >= 0)
        //    {
        //        if (Casillas[casilla] != null)//TODO, Convertir coordenada a pos arreglo
        //        {
        //            // Casilla no está vacía
        //            Console.WriteLine("Error: Casilla " + casilla + " no vacía (Contiene la Ficha " + Casillas[casilla] + ").");
        //            return false;
        //        }
        //        //else if (Fichas[_fichapasada] == OFF_BOARD || estadoficha[passedPiece] == IN_PLAY)
        //        //{
        //        //    // problem with the passedPiece
        //        //    System.out.println("Error: Piece " + passedPiece + " status is " + ((estadoficha[passedPiece] == OFF_BOARD) ? "off-board" : "in-play") + ".");
        //        //    return false;
        //        //}
        //        else
        //        {
        //            int posfila = _fichapasada % 4;
        //            int poscolumna = _fichapasada / 4;


        //            Casillas[posfila][poscolumna] = Fichas[_fichapasada];
        //            Fichas[_fichapasada].Disponible = false;
        //            nCasillasVacias--;
        //            _fichapasada = 16;           // no piece in limbo anymore, for the moment

        //            return true;
        //        }
        //    }
        //    else
        //    {
        //        Console.WriteLine("Argumento erróneo  pasado a Jugar() " + casilla + " fuera de los límites");
        //        return false;
        //    }
        //}

        public bool VerificarQuarto(int casilla)
        {
            bool checkRow = true;
            //bool checkCol = true;

            //int rowhead = casilla - (casilla % 4);
            //int colhead = casilla % 4;

            //Ficha r1 = Casillas[rowhead][colhead], r2 = Casillas[rowhead + 1],
            //    r3 = board[rowhead + 2], r4 = board[rowhead + 3];
            //int c1 = board[colhead], c2 = board[colhead + 4],
            //    c3 = board[colhead + 8], c4 = board[colhead + 12];

            //// check for empty spots in row or col
            //if (r1 == 16 || r2 == 16 ||
            //     r3 == 16 || r4 == 16)
            //{
            //    checkRow = false;
            //}
            //if (c1 == 16 || c2 == 16 ||
            //     c3 == 16 || c4 == 16)
            //{
            //    checkCol = false;
            //}
            //if (!checkRow && !checkCol)
            //{
            //    return false;
            //}

            //// no empty spots, start comparing pieces in row and col
            //int result;

            //// check rows
            //if (checkRow)
            //{
            //    result = QPiece.comparePieces(r1, r2, r3, r4);
            //    if (result < 4)
            //        return true;
            //}

            //// check columns
            //if (checkCol)
            //{
            //    result = QPiece.comparePieces(c1, c2, c3, c4);
            //    if (result < 4)
            //        return true;
            //}

            // nothing found
            return false;
        }

        public bool Pasar(int ficha)
        {
            //Establece la ficha pasada

            if (ficha < 0 || ficha > 15)
            {
                Console.WriteLine("Error: Pieza no existe.");
                return false;
            }

            if (Fichas[ficha].Disponible == true)
            {
                _fichapasada = ficha;
                Fichas[ficha].Disponible = false;      // Ficha ya o disponible
                nFichasSinJugar--;

                return true;
            }
            else
            {
                Console.WriteLine("Error pasando " + ficha + ". Ficha  no disponible.");
                return false;
            }
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}